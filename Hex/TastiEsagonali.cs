﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using Globals;

namespace Hex
{
    public class TastiEsagonali
    {
        public int xCentro, yCentro, mode;
        int raggio;
        int delta;
        int numTracce;
        public enum Stato { Attivo, Inattivo, Trasparente, Temporaneo }
        public Stato stato;
        public Point[,] vertici;        // SE METTO LA PAROLA CHIAVE "PUBLIC" POSSO CHIAMARLI IN FORM1 TRAMITE T.VERTICI

        Point[,] vertici1;
        Point[,] vertici2;

        public GraphicsPath[] perimetri;        // il vettore perimetri rappresenta i perimetri di tutti gli esagoni concentrici di un determinato tasto (perimetri[0] è il perimetro dell'esagono + interno)

        public GraphicsPath[,,] perimetri1;       // i perimetri dei trapezi che si formano tra un lato dell'esagono piu interno e quello successivo
        public GraphicsPath[,,] perimetri2;
        public GraphicsPath[,,] trapezio = new GraphicsPath [6,3,5];   // Num settori = 6, num_tracce = 3, num_max_triangolini interni = 5

        public string[] settoreA, settoreB, settoreC, settoreD, settoreE, settoreF;
        public Font f;
        Point Centro;
        public int settore, traccia, triangolo;
        GraphicsPath settore1, settore2, settore3, settore4, settore5, settore6;
        public SizeF dimensioneTesto;
        public PointF posizioneTesto= new PointF();
        Point posizioneCorrente;
        public int newSettore, newTraccia, newTriangolo;      // mi servono anche in form1
        public static int tastiAttivi = 0;
        static int tastiTemporanei = 0;
        public string[,][] testo=new string[3,6][];        // testo[mode][settori][nTracce] = letto nel file config     i settori sono predef (6) mentre le tracce sono lette nel file config

        Color coloreFont = Form1.getColoreFont();

        string coloreTestoMobile = "green"; //XYXYXY e uso 
        string coloreTestoTrasp = "black";  //XYXYXY e uso

        //creo tasto e tutto il reticolato                                                            // sono array di stringhe perchè il settore contiene la stringa di ogni traccia
        public TastiEsagonali(int xCentro, int yCentro, int raggio, int delta, int numTracce, int mode, Font f, string[] settoreA,string[] settoreB,string[] settoreC,string[] settoreD, string[] settoreE, string[] settoreF)
        {
            this.xCentro = xCentro;
            this.yCentro = yCentro;
            this.raggio = raggio;
            this.delta = delta;
            this.numTracce = numTracce;
            this.mode = mode;
            this.settoreA = settoreA;
            this.settoreB = settoreB;
            this.settoreC = settoreC;
            this.settoreD = settoreD;
            this.settoreE = settoreE;
            this.settoreF = settoreF;
            testo[mode,0] = settoreA;        // l'altro[] è definito dalla traccia   testo [mode,settori][nTriang]
            testo[mode,1] = settoreB;
            testo[mode,2] = settoreC;
            testo[mode,3] = settoreD;
            testo[mode,4] = settoreE;
            testo[mode,5] = settoreF;

            // gestione stinga speciale per passaggio modalità
            if (testo[mode,4][4] == "/>")
            {
                for (int x = 4; x < 9; x++)
                {
                    testo[mode, 4][x] = "→";
                }
            }

            //l'accento crea dei problemi nel momento della visualizzazione
            /*if (testo[mode,0][0] == "a")
            {
                testo[mode,4][4] = "à";
                testo[mode,4][5] = "à";
                testo[mode,4][6] = "à";
                testo[mode,4][7] = "à";
                testo[mode,4][8] = "à";
            }
            if (testo[mode,0][0] == "e")
            {
                testo[mode,4][4] = "è";
                testo[mode,4][5] = "è";
                testo[mode,4][6] = "è";
                testo[mode,4][7] = "è";
                testo[mode,4][8] = "è";
            }
            if (testo[mode,0][0] == "i")
            {
                testo[mode,4][4] = "ì";
                testo[mode,4][5] = "ì";
                testo[mode,4][6] = "ì";
                testo[mode,4][7] = "ì";
                testo[mode,4][8] = "ì";
            }
            if (testo[mode,0][0] == "o")
            {
                testo[mode,4][4] = "ò";
                testo[mode,4][5] = "ò";
                testo[mode,4][6] = "ò";
                testo[mode,4][7] = "ò";
                testo[mode,4][8] = "ò";
            }
            if (testo[mode,0][0] == "u")
            {
                testo[mode,4][4] = "ù";
                testo[mode,4][5] = "ù";
                testo[mode,4][6] = "ù";
                testo[mode,4][7] = "ù";
                testo[mode,4][8] = "ù";
            }   */    

            this.f = f;
            stato = Stato.Inattivo;
            vertici = new Point[6, numTracce];          // 6 = num settori, sta settando la dimensione della matrice
            perimetri = new GraphicsPath[numTracce];    // crea tanti esagoni concentrici quante sono le tracce, lo fa nel for
                
            vertici1 = new Point[6, numTracce];     // vertici che verranno presi a meta dei lati degli esagoni successivi per costruire i triangolini
            perimetri1 = new GraphicsPath[6, numTracce, 3];   // perimetri1[settore,numTracce,numTriangolini]   PER ORA CI OCCUPIAMO SOLO DI 3 TRIANGOLINI

            if (numTracce %2 == 0)      // se il numTracce è divisibile per 2
                vertici2 = new Point[6 * numTracce, numTracce];            // punti triangolini trapezi traccia 2
            for (int i = 1; i < 1000; i++)
            {
                if (numTracce == (2 * i) + 1)
                {
                    vertici2 = new Point[6 * (numTracce - 1), numTracce];            // punti triangolini trapezi traccia 2
                }
            }

            perimetri2 = new GraphicsPath[6, numTracce, (2*numTracce) +1];     // perimetri triangolini traccia 2

            for (int i = 0; i < numTracce; i++)         // ogni ciclo un esagono
            {
                
                // per trovare i punti medi ho usato la formula (a+b)/2
                vertici[0, i] = new Point((xCentro - (raggio + delta * i)), yCentro);       // crea i vertici dell'esagono...
                    vertici2[0, i] = new Point((int)((xCentro - raggio - (raggio + delta * i) + (int)(xCentro - (raggio + delta * (i-1)) / 2)) / 2), (int)(yCentro - (raggio + delta * (i - 2)) * 0.866));
                  vertici1[0, i] = new Point((int)((xCentro - (raggio + delta * i) + (int)(xCentro - (raggio + delta * i) / 2)) / 2), ((yCentro + (int)(yCentro - (raggio + delta * i) * 0.866))/2));       // crea il pto a meta del lato dell'esagono successivo dell'esagono...
                    vertici2[1, i] = new Point((xCentro - (raggio + delta * (i-1))), (int)(yCentro - (raggio + delta * (i - 1)) * 0.866));
                vertici[1, i] = new Point((int)(xCentro - (raggio + delta * i) / 2), (int)(yCentro - (raggio + delta * i) * 0.866));
                    vertici2[2, i] = new Point((int)((xCentro - (raggio + delta * i) / 2) + raggio), (int)(yCentro - (raggio + delta * i) * 0.866));
                  vertici1[1, i] = new Point((int)(((int)(xCentro - (raggio + delta * i) / 2) + (int)(xCentro + (raggio + delta * i) / 2)) / 2), (int)(yCentro - (raggio + delta * i) * 0.866));
                    vertici2[3, i] = new Point((int)((xCentro - (raggio + delta * i) / 2) + 2 * raggio), (int)(yCentro - (raggio + delta * i) * 0.866));
                vertici[2, i] = new Point((int)(xCentro + (raggio + delta * i) / 2), (int)(yCentro - (raggio + delta * i) * 0.866));
                    vertici2[4, i] = new Point(xCentro +(2*raggio),(int)(yCentro - (raggio + delta * (i-1)) * 0.866));
                  vertici1[2,i] = new Point((int)(((int)(xCentro + (raggio + delta * i) / 2) + (int)(xCentro + (raggio + delta * i)))/2),(((int)(yCentro - (raggio + delta * i) * 0.866)+ yCentro)/2));
                    vertici2[5, i] = new Point(xCentro + (2*raggio) + (raggio/2),(int)(yCentro - (raggio + delta * (i-2)) * 0.866));
                vertici[3, i] = new Point((xCentro + (raggio + delta * i)), yCentro);
                    vertici2[6, i] = new Point(xCentro + (2*raggio) + (raggio/2),(int)(yCentro + (raggio + delta * (i-2)) * 0.866));
                  vertici1[3,i] = new Point((int)(((xCentro + (raggio + delta * i)) + (int)(xCentro + (raggio + delta * i) / 2))/2), (int)((yCentro+(int)(yCentro + (raggio + delta * i) * 0.866))/2));
                    vertici2[7, i] = new Point(xCentro + (2 * raggio), (int)(yCentro + (raggio + delta * (i - 1)) * 0.866));
                vertici[4, i] = new Point((int)(xCentro + (raggio + delta * i) / 2), (int)(yCentro + (raggio + delta * i) * 0.866));
                    vertici2[8, i] = new Point(xCentro + (raggio/2),(int)(yCentro + (raggio + delta * i) * 0.866));
                  vertici1[4,i] = new Point(xCentro,((int)(yCentro + (raggio + delta * i) * 0.866)));
                    vertici2[9, i] = new Point(xCentro - (raggio / 2), (int)(yCentro + (raggio + delta * i) * 0.866));
                vertici[5, i] = new Point((int)(xCentro - (raggio + delta * i) / 2), (int)(yCentro + (raggio + delta * i) * 0.866));
                    vertici2[10, i] = new Point((xCentro - (raggio + delta * (i - 1))), (int)(yCentro + (raggio + delta * (i - 1)) * 0.866));
                  vertici1[5,i] = new Point((int)(((xCentro- (raggio + delta * i)) +(int)(xCentro - (raggio + delta * i) / 2))/2),(int)((int)(((yCentro + (raggio + delta * i) * 0.866) + yCentro)/2)));
                    vertici2[11, i] = new Point((int)((xCentro - raggio - (raggio + delta * i) + (int)(xCentro - (raggio + delta * (i-1)) / 2)) / 2),(int)(yCentro + (raggio + delta * (i-2)) * 0.866));

                perimetri[i] = new GraphicsPath();
                perimetri[i].AddLine(vertici[0, i], vertici[1, i]);         // ...e li unisce
                perimetri[i].AddLine(vertici[1, i], vertici[2, i]);
                perimetri[i].AddLine(vertici[2, i], vertici[3, i]);
                perimetri[i].AddLine(vertici[3, i], vertici[4, i]);
                perimetri[i].AddLine(vertici[4, i], vertici[5, i]);
                perimetri[i].AddLine(vertici[5, i], vertici[0, i]);
            }

            
            
            #region Creazione tre triangolini a settore traccia 1
            if (numTracce >= 2)
            {
                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[0, 1, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri1[0, 1, i].AddLine(vertici[0, 0], vertici[0, 1]);
                        perimetri1[0, 1, i].AddLine(vertici[0, 1], vertici1[0, 1]);
                        perimetri1[0, 1, i].AddLine(vertici1[0, 1], vertici[0, 0]);
                    }

                    if (i == 1)
                    {
                        perimetri1[0, 1, i].AddLine(vertici[0, 0], vertici1[0, 1]);
                        perimetri1[0, 1, i].AddLine(vertici1[0, 1], vertici[1, 0]);
                        perimetri1[0, 1, i].AddLine(vertici[1, 0], vertici[0, 0]);
                    }

                    if (i == 2)
                    {
                        perimetri1[0, 1, i].AddLine(vertici[1, 0], vertici1[0, 1]);
                        perimetri1[0, 1, i].AddLine(vertici1[0, 1], vertici[1, 1]);
                        perimetri1[0, 1, i].AddLine(vertici[1, 1], vertici[1, 0]);
                    }
                }

                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[1, 1, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri1[1, 1, i].AddLine(vertici[1, 0], vertici[1, 1]);
                        perimetri1[1, 1, i].AddLine(vertici[1, 1], vertici1[1, 1]);
                        perimetri1[1, 1, i].AddLine(vertici1[1, 1], vertici[1, 0]);
                    }

                    if (i == 1)
                    {
                        perimetri1[1, 1, i].AddLine(vertici[1, 0], vertici1[1, 1]);
                        perimetri1[1, 1, i].AddLine(vertici1[1, 1], vertici[2, 0]);
                        perimetri1[1, 1, i].AddLine(vertici[2, 0], vertici[1, 0]);
                    }

                    if (i == 2)
                    {
                        perimetri1[1, 1, i].AddLine(vertici[2, 0], vertici1[1, 1]);
                        perimetri1[1, 1, i].AddLine(vertici1[1, 1], vertici[2, 1]);
                        perimetri1[1, 1, i].AddLine(vertici[2, 1], vertici[2, 0]);
                    }
                }

                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[2, 1, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri1[2, 1, i].AddLine(vertici[2, 0], vertici[2, 1]);
                        perimetri1[2, 1, i].AddLine(vertici[2, 1], vertici1[2, 1]);
                        perimetri1[2, 1, i].AddLine(vertici1[2, 1], vertici[2, 0]);
                    }

                    if (i == 1)
                    {
                        perimetri1[2, 1, i].AddLine(vertici[2, 0], vertici1[2, 1]);
                        perimetri1[2, 1, i].AddLine(vertici1[2, 1], vertici[3, 0]);
                        perimetri1[2, 1, i].AddLine(vertici[3, 0], vertici[2, 0]);
                    }

                    if (i == 2)
                    {
                        perimetri1[2, 1, i].AddLine(vertici[3, 0], vertici1[2, 1]);
                        perimetri1[2, 1, i].AddLine(vertici1[2, 1], vertici[3, 1]);
                        perimetri1[2, 1, i].AddLine(vertici[3, 1], vertici[3, 0]);
                    }
                }

                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[3, 1, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri1[3, 1, i].AddLine(vertici[3, 0], vertici[3, 1]);
                        perimetri1[3, 1, i].AddLine(vertici[3, 1], vertici1[3, 1]);
                        perimetri1[3, 1, i].AddLine(vertici1[3, 1], vertici[3, 0]);
                    }

                    if (i == 1)
                    {
                        perimetri1[3, 1, i].AddLine(vertici[3, 0], vertici1[3, 1]);
                        perimetri1[3, 1, i].AddLine(vertici1[3, 1], vertici[4, 0]);
                        perimetri1[3, 1, i].AddLine(vertici[4, 0], vertici[3, 0]);
                    }

                    if (i == 2)
                    {
                        perimetri1[3, 1, i].AddLine(vertici[4, 0], vertici1[3, 1]);
                        perimetri1[3, 1, i].AddLine(vertici1[3, 1], vertici[4, 1]);
                        perimetri1[3, 1, i].AddLine(vertici[4, 1], vertici[4, 0]);
                    }
                }

                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[4, 1, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri1[4, 1, i].AddLine(vertici[4, 0], vertici[4, 1]);
                        perimetri1[4, 1, i].AddLine(vertici[4, 1], vertici1[4, 1]);
                        perimetri1[4, 1, i].AddLine(vertici1[4, 1], vertici[4, 0]);
                    }

                    if (i == 1)
                    {
                        perimetri1[4, 1, i].AddLine(vertici[4, 0], vertici1[4, 1]);
                        perimetri1[4, 1, i].AddLine(vertici1[4, 1], vertici[5, 0]);
                        perimetri1[4, 1, i].AddLine(vertici[5, 0], vertici[4, 0]);
                    }

                    if (i == 2)
                    {
                        perimetri1[4, 1, i].AddLine(vertici[5, 0], vertici1[4, 1]);
                        perimetri1[4, 1, i].AddLine(vertici1[4, 1], vertici[5, 1]);
                        perimetri1[4, 1, i].AddLine(vertici[5, 1], vertici[5, 0]);
                    }
                }

                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[5, 1, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri1[5, 1, i].AddLine(vertici[5, 0], vertici[5, 1]);
                        perimetri1[5, 1, i].AddLine(vertici[5, 1], vertici1[5, 1]);
                        perimetri1[5, 1, i].AddLine(vertici1[5, 1], vertici[5, 0]);
                    }

                    if (i == 1)
                    {
                        perimetri1[5, 1, i].AddLine(vertici[5, 0], vertici1[5, 1]);
                        perimetri1[5, 1, i].AddLine(vertici1[5, 1], vertici[0, 0]);
                        perimetri1[5, 1, i].AddLine(vertici[0, 0], vertici[5, 0]);
                    }

                    if (i == 2)
                    {
                        perimetri1[5, 1, i].AddLine(vertici[0, 0], vertici1[5, 1]);
                        perimetri1[5, 1, i].AddLine(vertici1[5, 1], vertici[0, 1]);
                        perimetri1[5, 1, i].AddLine(vertici[0, 1], vertici[0, 0]);
                    }
                }
            }
            #endregion

            #region Creazione cinque triangolini a settore traccia 2
            if (numTracce >= 3)
            {
                for (int i = 0; i < 5; i++)
                {
                    perimetri2[0, 2, i] = new GraphicsPath();  // per creare i triangolini      //DA PROBLEMI CON LA LETTURA DELLA TASTIERA... dovevo metter if( numTracce >=3) altrimenti la leggeva anche con 2 tracce ma in questo caso i 5 triangolini non saranno visualizz mai -> nn li creo

                    if (i == 0)
                    {
                        perimetri2[0, 2, i].AddLine(vertici[0, 1], vertici[0, 2]);
                        perimetri2[0, 2, i].AddLine(vertici[0, 2], vertici2[0, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[0, 2], vertici[0, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri2[0, 2, i].AddLine(vertici[0, 1], vertici2[0, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[0, 2], vertici1[0, 1]);
                        perimetri2[0, 2, i].AddLine(vertici1[0, 1], vertici[0, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[0, 2, i].AddLine(vertici1[0, 1], vertici2[0, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[0, 2], vertici2[1, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[1, 2], vertici1[0, 1]);
                    }

                    if (i == 3)
                    {
                        perimetri2[0, 2, i].AddLine(vertici1[0, 1], vertici2[1, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[1, 2], vertici[1, 1]);
                        perimetri2[0, 2, i].AddLine(vertici[1, 1], vertici1[0, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[0, 2, i].AddLine(vertici[1, 1], vertici2[1, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[1, 2], vertici[1, 2]);
                        perimetri2[0, 2, i].AddLine(vertici[1, 2], vertici[1, 1]);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    perimetri2[1, 2, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri2[1, 2, i].AddLine(vertici[1, 1], vertici[1, 2]);
                        perimetri2[1, 2, i].AddLine(vertici[1, 2], vertici2[2, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[2, 2], vertici[1, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri2[1, 2, i].AddLine(vertici[1, 1], vertici2[2, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[2, 2], vertici1[1, 1]);
                        perimetri2[1, 2, i].AddLine(vertici1[1, 1], vertici[1, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[1, 2, i].AddLine(vertici1[1, 1], vertici2[2, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[2, 2], vertici2[3, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[3, 2], vertici1[1, 1]);
                    }

                    if (i == 3)
                    {
                        perimetri2[1, 2, i].AddLine(vertici1[1, 1], vertici2[3, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[3, 2], vertici[2, 1]);
                        perimetri2[1, 2, i].AddLine(vertici[2, 1], vertici1[1, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[1, 2, i].AddLine(vertici[2, 1], vertici2[3, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[3, 2], vertici[2, 2]);
                        perimetri2[1, 2, i].AddLine(vertici[2, 2], vertici[2, 1]);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    perimetri2[2, 2, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri2[2, 2, i].AddLine(vertici[2, 1], vertici[2, 2]);
                        perimetri2[2, 2, i].AddLine(vertici[2, 2], vertici2[4, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[4, 2], vertici[2, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri2[2, 2, i].AddLine(vertici[2, 1], vertici2[4, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[4, 2], vertici1[2, 1]);
                        perimetri2[2, 2, i].AddLine(vertici1[2, 1], vertici[2, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[2, 2, i].AddLine(vertici1[2, 1], vertici2[4, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[4, 2], vertici2[5, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[5, 2], vertici1[2, 1]);
                    }

                    if (i == 3)
                    {
                        perimetri2[2, 2, i].AddLine(vertici1[2, 1], vertici2[5, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[5, 2], vertici[3, 1]);
                        perimetri2[2, 2, i].AddLine(vertici[3, 1], vertici1[2, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[2, 2, i].AddLine(vertici[3, 1], vertici2[5, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[5, 2], vertici[3, 2]);
                        perimetri2[2, 2, i].AddLine(vertici[3, 2], vertici[3, 1]);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    perimetri2[3, 2, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri2[3, 2, i].AddLine(vertici[3, 1], vertici[3, 2]);
                        perimetri2[3, 2, i].AddLine(vertici[3, 2], vertici2[6, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[6, 2], vertici[3, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri2[3, 2, i].AddLine(vertici[3, 1], vertici2[6, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[6, 2], vertici1[3, 1]);
                        perimetri2[3, 2, i].AddLine(vertici1[3, 1], vertici[3, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[3, 2, i].AddLine(vertici1[3, 1], vertici2[6, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[6, 2], vertici2[7, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[7, 2], vertici1[3, 1]);
                    }

                    if (i == 3)
                    {
                        perimetri2[3, 2, i].AddLine(vertici1[3, 1], vertici2[7, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[7, 2], vertici[4, 1]);
                        perimetri2[3, 2, i].AddLine(vertici[4, 1], vertici1[3, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[3, 2, i].AddLine(vertici[4, 1], vertici2[7, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[7, 2], vertici[4, 2]);
                        perimetri2[3, 2, i].AddLine(vertici[4, 2], vertici[4, 1]);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    perimetri2[4, 2, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri2[4, 2, i].AddLine(vertici[4, 1], vertici[4, 2]);
                        perimetri2[4, 2, i].AddLine(vertici[4, 2], vertici2[8, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[8, 2], vertici[4, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri2[4, 2, i].AddLine(vertici[4, 1], vertici2[8, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[8, 2], vertici1[4, 1]);
                        perimetri2[4, 2, i].AddLine(vertici1[4, 1], vertici[4, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[4, 2, i].AddLine(vertici1[4, 1], vertici2[8, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[8, 2], vertici2[9, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[9, 2], vertici1[4, 1]);
                    }

                    if (i == 3)
                    {
                        perimetri2[4, 2, i].AddLine(vertici1[4, 1], vertici2[9, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[9, 2], vertici[5, 1]);
                        perimetri2[4, 2, i].AddLine(vertici[5, 1], vertici1[4, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[4, 2, i].AddLine(vertici[5, 1], vertici2[9, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[9, 2], vertici[5, 2]);
                        perimetri2[4, 2, i].AddLine(vertici[5, 2], vertici[5, 1]);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    perimetri2[5, 2, i] = new GraphicsPath();  // per creare i triangolini

                    if (i == 0)
                    {
                        perimetri2[5, 2, i].AddLine(vertici[5, 1], vertici[5, 2]);
                        perimetri2[5, 2, i].AddLine(vertici[5, 2], vertici2[10, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[10, 2], vertici[5, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri2[5, 2, i].AddLine(vertici[5, 1], vertici2[10, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[10, 2], vertici1[5, 1]);
                        perimetri2[5, 2, i].AddLine(vertici1[5, 1], vertici[5, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[5, 2, i].AddLine(vertici1[5, 1], vertici2[10, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[10, 2], vertici2[11, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[11, 2], vertici1[5, 1]);
                    }

                    if (i == 3)
                    {
                        perimetri2[5, 2, i].AddLine(vertici1[5, 1], vertici2[11, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[11, 2], vertici[0, 1]);
                        perimetri2[5, 2, i].AddLine(vertici[0, 1], vertici1[5, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[5, 2, i].AddLine(vertici[0, 1], vertici2[11, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[11, 2], vertici[0, 2]);
                        perimetri2[5, 2, i].AddLine(vertici[0, 2], vertici[0, 1]);
                    }
                }
            }
            #endregion
            
            
            Centro = new Point(xCentro, yCentro);   // il centro di tutti gli esagoni (essendo concentrici è lo stesso)
            settore = 0;
            traccia = 0;
            
            settore1 = new GraphicsPath();      // li mettiamo GraphicsPath così poi potremo vedere se il ptatore si trova all'interno di uno di loro
            settore2 = new GraphicsPath();
            settore3 = new GraphicsPath();
            settore4 = new GraphicsPath();
            settore5 = new GraphicsPath();
            settore6 = new GraphicsPath();
            
            settore1.AddLine(Centro, vertici[0,numTracce - 1]);     // unisce il centro con il vertice dell'esagono + esterno
            settore1.AddLine(vertici[0,numTracce - 1], vertici[1,numTracce - 1]);
            settore1.AddLine(vertici[1,numTracce - 1], Centro);
            
            settore2.AddLine(Centro, vertici[1,numTracce - 1]);     // crea degli spicchi ke corrispondono ai vari settori (v foglio)
            settore2.AddLine(vertici[1,numTracce - 1], vertici[2,numTracce - 1]);
            settore2.AddLine(vertici[2, numTracce - 1], Centro);
            
            settore3.AddLine(Centro, vertici[2,numTracce - 1]);
            settore3.AddLine(vertici[2,numTracce - 1], vertici[3,numTracce - 1]);
            settore3.AddLine(vertici[3,numTracce - 1], Centro);

                                //   1 2 
                                //  0   3
                                //   5 4

            settore4.AddLine(Centro, vertici[3,numTracce - 1]);
            settore4.AddLine(vertici[3,numTracce - 1], vertici[4,numTracce - 1]);
            settore4.AddLine(vertici[4,numTracce - 1], Centro);
            
            settore5.AddLine(Centro, vertici[4,numTracce - 1]);
            settore5.AddLine(vertici[4,numTracce - 1], vertici[5,numTracce - 1]);
            settore5.AddLine(vertici[5,numTracce - 1], Centro);
            
            settore6.AddLine(Centro, vertici[5,numTracce - 1]);
            settore6.AddLine(vertici[5,numTracce - 1], vertici[0,numTracce - 1]);
            settore6.AddLine(vertici[0,numTracce - 1], Centro);
        }

        public void UpdateMode(int mode)
        {
            this.mode = mode;
        }

        public void UpdateTastoEsag(int mode, string[] settoreA, string[] settoreB, string[] settoreC, string[] settoreD, string[] settoreE, string[] settoreF)
        {
            this.mode = mode;
            this.settoreA = settoreA;
            this.settoreB = settoreB;
            this.settoreC = settoreC;
            this.settoreD = settoreD;
            this.settoreE = settoreE;
            this.settoreF = settoreF;
            testo[mode, 0] = settoreA;        // l'altro[] è definito dalla traccia   testo [mode,settori][nTriang]
            testo[mode, 1] = settoreB;
            testo[mode, 2] = settoreC;
            testo[mode, 3] = settoreD;
            testo[mode, 4] = settoreE;
            testo[mode, 5] = settoreF;

            // gestione stinga speciale per passaggio modalità
            if (testo[mode, 4][1] == "/>")
            {
                for(int x=1; x<9; x++)
                    testo[mode, 4][x] = "→";
            }

            //l'accento crea dei problemi nel momento della visualizzazione
            /*if (testo[mode, 0][0] == "a")
            {
                testo[mode, 4][4] = "à";
                testo[mode, 4][5] = "à";
                testo[mode, 4][6] = "à";
                testo[mode, 4][7] = "à";
                testo[mode, 4][8] = "à";
            }
            if (testo[mode, 0][0] == "e")
            {
                testo[mode, 4][4] = "è";
                testo[mode, 4][5] = "è";
                testo[mode, 4][6] = "è";
                testo[mode, 4][7] = "è";
                testo[mode, 4][8] = "è";
            }
            if (testo[mode, 0][0] == "i")
            {
                testo[mode, 4][4] = "ì";
                testo[mode, 4][5] = "ì";
                testo[mode, 4][6] = "ì";
                testo[mode, 4][7] = "ì";
                testo[mode, 4][8] = "ì";
            }
            if (testo[mode, 0][0] == "o")
            {
                testo[mode, 4][4] = "ò";
                testo[mode, 4][5] = "ò";
                testo[mode, 4][6] = "ò";
                testo[mode, 4][7] = "ò";
                testo[mode, 4][8] = "ò";
            }
            if (testo[mode, 0][0] == "u")
            {
                testo[mode, 4][4] = "ù";
                testo[mode, 4][5] = "ù";
                testo[mode, 4][6] = "ù";
                testo[mode, 4][7] = "ù";
                testo[mode, 4][8] = "ù";
            }     */  
        }

        public void ModTastEsag(float dim, float dimcar)
        {
            this.xCentro =Convert.ToInt32(xCentro*dim);
            this.yCentro =Convert.ToInt32(yCentro*dim);
            this.raggio = Convert.ToInt32(raggio*dim);
            this.delta =Convert.ToInt32( delta*dim);
            f = new Font(FontFamily.GenericSansSerif, dimcar * dim);

            for (int i = 0; i < numTracce; i++)         // ogni ciclo un esagono
            {
                // per trovare i punti medi ho usato la formula (a+b)/2
                vertici[0, i] = new Point((xCentro - (raggio + delta * i)), yCentro);       // crea i vertici dell'esagono...
                vertici2[0, i] = new Point((int)((xCentro - raggio - (raggio + delta * i) + (int)(xCentro - (raggio + delta * (i - 1)) / 2)) / 2), (int)(yCentro - (raggio + delta * (i - 2)) * 0.866));
                vertici1[0, i] = new Point((int)((xCentro - (raggio + delta * i) + (int)(xCentro - (raggio + delta * i) / 2)) / 2), ((yCentro + (int)(yCentro - (raggio + delta * i) * 0.866)) / 2));       // crea il pto a meta del lato dell'esagono successivo dell'esagono...
                vertici2[1, i] = new Point((xCentro - (raggio + delta * (i - 1))), (int)(yCentro - (raggio + delta * (i - 1)) * 0.866));
                vertici[1, i] = new Point((int)(xCentro - (raggio + delta * i) / 2), (int)(yCentro - (raggio + delta * i) * 0.866));
                vertici2[2, i] = new Point((int)((xCentro - (raggio + delta * i) / 2) + raggio), (int)(yCentro - (raggio + delta * i) * 0.866));
                vertici1[1, i] = new Point((int)(((int)(xCentro - (raggio + delta * i) / 2) + (int)(xCentro + (raggio + delta * i) / 2)) / 2), (int)(yCentro - (raggio + delta * i) * 0.866));
                vertici2[3, i] = new Point((int)((xCentro - (raggio + delta * i) / 2) + 2 * raggio), (int)(yCentro - (raggio + delta * i) * 0.866));
                vertici[2, i] = new Point((int)(xCentro + (raggio + delta * i) / 2), (int)(yCentro - (raggio + delta * i) * 0.866));
                vertici2[4, i] = new Point(xCentro + (2 * raggio), (int)(yCentro - (raggio + delta * (i - 1)) * 0.866));
                vertici1[2, i] = new Point((int)(((int)(xCentro + (raggio + delta * i) / 2) + (int)(xCentro + (raggio + delta * i))) / 2), (((int)(yCentro - (raggio + delta * i) * 0.866) + yCentro) / 2));
                vertici2[5, i] = new Point(xCentro + (2 * raggio) + (raggio / 2), (int)(yCentro - (raggio + delta * (i - 2)) * 0.866));
                vertici[3, i] = new Point((xCentro + (raggio + delta * i)), yCentro);
                vertici2[6, i] = new Point(xCentro + (2 * raggio) + (raggio / 2), (int)(yCentro + (raggio + delta * (i - 2)) * 0.866));
                vertici1[3, i] = new Point((int)(((xCentro + (raggio + delta * i)) + (int)(xCentro + (raggio + delta * i) / 2)) / 2), (int)((yCentro + (int)(yCentro + (raggio + delta * i) * 0.866)) / 2));
                vertici2[7, i] = new Point(xCentro + (2 * raggio), (int)(yCentro + (raggio + delta * (i - 1)) * 0.866));
                vertici[4, i] = new Point((int)(xCentro + (raggio + delta * i) / 2), (int)(yCentro + (raggio + delta * i) * 0.866));
                vertici2[8, i] = new Point(xCentro + (raggio / 2), (int)(yCentro + (raggio + delta * i) * 0.866));
                vertici1[4, i] = new Point(xCentro, ((int)(yCentro + (raggio + delta * i) * 0.866)));
                vertici2[9, i] = new Point(xCentro - (raggio / 2), (int)(yCentro + (raggio + delta * i) * 0.866));
                vertici[5, i] = new Point((int)(xCentro - (raggio + delta * i) / 2), (int)(yCentro + (raggio + delta * i) * 0.866));
                vertici2[10, i] = new Point((xCentro - (raggio + delta * (i - 1))), (int)(yCentro + (raggio + delta * (i - 1)) * 0.866));
                vertici1[5, i] = new Point((int)(((xCentro - (raggio + delta * i)) + (int)(xCentro - (raggio + delta * i) / 2)) / 2), (int)((int)(((yCentro + (raggio + delta * i) * 0.866) + yCentro) / 2)));
                vertici2[11, i] = new Point((int)((xCentro - raggio - (raggio + delta * i) + (int)(xCentro - (raggio + delta * (i - 1)) / 2)) / 2), (int)(yCentro + (raggio + delta * (i - 2)) * 0.866));

                perimetri[i] = new GraphicsPath();
                perimetri[i].AddLine(vertici[0, i], vertici[1, i]);         // ...e li unisce
                perimetri[i].AddLine(vertici[1, i], vertici[2, i]);
                perimetri[i].AddLine(vertici[2, i], vertici[3, i]);
                perimetri[i].AddLine(vertici[3, i], vertici[4, i]);
                perimetri[i].AddLine(vertici[4, i], vertici[5, i]);
                perimetri[i].AddLine(vertici[5, i], vertici[0, i]);
            }


            #region Creazione tre triangolini a settore traccia 1
            if (numTracce >= 2)
            {
                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[0, 1, i] = new GraphicsPath();

                    trapezio[0, 1, i] = new GraphicsPath();

                    //BBB
                    if (i == 0)
                    {
                        perimetri1[0, 1, i].AddLine(vertici[0, 0], vertici[0, 1]);
                        perimetri1[0, 1, i].AddLine(vertici[0, 1], vertici1[0, 1]);
                        perimetri1[0, 1, i].AddLine(vertici1[0, 1], vertici[0, 0]);
                        trapezio[0, 1, i].AddLine(vertici[0, 0], vertici[0, 1]);
                        trapezio[0, 1, i].AddLine(vertici[0, 1], vertici1[0, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri1[0, 1, i].AddLine(vertici[0, 0], vertici1[0, 1]);
                        perimetri1[0, 1, i].AddLine(vertici1[0, 1], vertici[1, 0]);
                        perimetri1[0, 1, i].AddLine(vertici[1, 0], vertici[0, 0]);
                        trapezio[0, 1, i].AddLine(vertici[1, 0], vertici[0, 0]);
                    }
                    
                    if (i == 2)
                    {
                        perimetri1[0, 1, i].AddLine(vertici[1, 0], vertici1[0, 1]);
                        perimetri1[0, 1, i].AddLine(vertici1[0, 1], vertici[1, 1]);
                        perimetri1[0, 1, i].AddLine(vertici[1, 1], vertici[1, 0]);
                        trapezio[0, 1, i].AddLine(vertici1[0, 1], vertici[1, 1]);
                        trapezio[0, 1, i].AddLine(vertici[1, 1], vertici[1, 0]);
                    }
                }

                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[1, 1, i] = new GraphicsPath();  // per creare i triangolini
                    trapezio[1, 1, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri1[1, 1, i].AddLine(vertici[1, 0], vertici[1, 1]);
                        perimetri1[1, 1, i].AddLine(vertici[1, 1], vertici1[1, 1]);
                        perimetri1[1, 1, i].AddLine(vertici1[1, 1], vertici[1, 0]);
                        trapezio[1, 1, i].AddLine(vertici[1, 0], vertici[1, 1]);
                        trapezio[1, 1, i].AddLine(vertici[1, 1], vertici1[1, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri1[1, 1, i].AddLine(vertici[1, 0], vertici1[1, 1]);
                        perimetri1[1, 1, i].AddLine(vertici1[1, 1], vertici[2, 0]);
                        perimetri1[1, 1, i].AddLine(vertici[2, 0], vertici[1, 0]);
                        trapezio[1, 1, i].AddLine(vertici[2, 0], vertici[1, 0]);
                    }
                    
                    if (i == 2)
                    {
                        perimetri1[1, 1, i].AddLine(vertici[2, 0], vertici1[1, 1]);
                        perimetri1[1, 1, i].AddLine(vertici1[1, 1], vertici[2, 1]);
                        perimetri1[1, 1, i].AddLine(vertici[2, 1], vertici[2, 0]);
                        trapezio[1, 1, i].AddLine(vertici1[1, 1], vertici[2, 1]);
                        trapezio[1, 1, i].AddLine(vertici[2, 1], vertici[2, 0]);
                    }
                }

                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[2, 1, i] = new GraphicsPath();  // per creare i triangolini
                    trapezio[2, 1, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri1[2, 1, i].AddLine(vertici[2, 0], vertici[2, 1]);
                        perimetri1[2, 1, i].AddLine(vertici[2, 1], vertici1[2, 1]);
                        perimetri1[2, 1, i].AddLine(vertici1[2, 1], vertici[2, 0]);
                        trapezio[2, 1, i].AddLine(vertici[2, 0], vertici[2, 1]);
                        trapezio[2, 1, i].AddLine(vertici[2, 1], vertici1[2, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri1[2, 1, i].AddLine(vertici[2, 0], vertici1[2, 1]);
                        perimetri1[2, 1, i].AddLine(vertici1[2, 1], vertici[3, 0]);
                        perimetri1[2, 1, i].AddLine(vertici[3, 0], vertici[2, 0]);
                        trapezio[2, 1, i].AddLine(vertici[3, 0], vertici[2, 0]);
                    }

                    if (i == 2)
                    {
                        perimetri1[2, 1, i].AddLine(vertici[3, 0], vertici1[2, 1]);
                        perimetri1[2, 1, i].AddLine(vertici1[2, 1], vertici[3, 1]);
                        perimetri1[2, 1, i].AddLine(vertici[3, 1], vertici[3, 0]);
                        trapezio[2, 1, i].AddLine(vertici1[2, 1], vertici[3, 1]);
                        trapezio[2, 1, i].AddLine(vertici[3, 1], vertici[3, 0]);
                    }
                }

                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[3, 1, i] = new GraphicsPath();  // per creare i triangolini
                    trapezio[3, 1, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri1[3, 1, i].AddLine(vertici[3, 0], vertici[3, 1]);
                        perimetri1[3, 1, i].AddLine(vertici[3, 1], vertici1[3, 1]);
                        perimetri1[3, 1, i].AddLine(vertici1[3, 1], vertici[3, 0]);
                        trapezio[3, 1, i].AddLine(vertici[3, 0], vertici[3, 1]);
                        trapezio[3, 1, i].AddLine(vertici[3, 1], vertici1[3, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri1[3, 1, i].AddLine(vertici[3, 0], vertici1[3, 1]);
                        perimetri1[3, 1, i].AddLine(vertici1[3, 1], vertici[4, 0]);
                        perimetri1[3, 1, i].AddLine(vertici[4, 0], vertici[3, 0]);
                        trapezio[3, 1, i].AddLine(vertici[4, 0], vertici[3, 0]);
                    }

                    if (i == 2)
                    {
                        perimetri1[3, 1, i].AddLine(vertici[4, 0], vertici1[3, 1]);
                        perimetri1[3, 1, i].AddLine(vertici1[3, 1], vertici[4, 1]);
                        perimetri1[3, 1, i].AddLine(vertici[4, 1], vertici[4, 0]);
                        trapezio[3, 1, i].AddLine(vertici1[3, 1], vertici[4, 1]);
                        trapezio[3, 1, i].AddLine(vertici[4, 1], vertici[4, 0]);
                    }
                }

                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[4, 1, i] = new GraphicsPath();  // per creare i triangolini
                    trapezio[4, 1, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri1[4, 1, i].AddLine(vertici[4, 0], vertici[4, 1]);
                        perimetri1[4, 1, i].AddLine(vertici[4, 1], vertici1[4, 1]);
                        perimetri1[4, 1, i].AddLine(vertici1[4, 1], vertici[4, 0]);
                        trapezio[4, 1, i].AddLine(vertici[4, 0], vertici[4, 1]);
                        trapezio[4, 1, i].AddLine(vertici[4, 1], vertici1[4, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri1[4, 1, i].AddLine(vertici[4, 0], vertici1[4, 1]);
                        perimetri1[4, 1, i].AddLine(vertici1[4, 1], vertici[5, 0]);
                        perimetri1[4, 1, i].AddLine(vertici[5, 0], vertici[4, 0]);
                        trapezio[4, 1, i].AddLine(vertici[5, 0], vertici[4, 0]);
                    }

                    if (i == 2)
                    {
                        perimetri1[4, 1, i].AddLine(vertici[5, 0], vertici1[4, 1]);
                        perimetri1[4, 1, i].AddLine(vertici1[4, 1], vertici[5, 1]);
                        perimetri1[4, 1, i].AddLine(vertici[5, 1], vertici[5, 0]);
                        trapezio[4, 1, i].AddLine(vertici1[4, 1], vertici[5, 1]);
                        trapezio[4, 1, i].AddLine(vertici[5, 1], vertici[5, 0]);
                    }
                }

                for (int i = 0; i < 3; i++)     // perimetri1[settore,numTracce,numTriangolini]
                {
                    perimetri1[5, 1, i] = new GraphicsPath();  // per creare i triangolini
                    trapezio[5, 1, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri1[5, 1, i].AddLine(vertici[5, 0], vertici[5, 1]);
                        perimetri1[5, 1, i].AddLine(vertici[5, 1], vertici1[5, 1]);
                        perimetri1[5, 1, i].AddLine(vertici1[5, 1], vertici[5, 0]);
                        trapezio[5, 1, i].AddLine(vertici[5, 0], vertici[5, 1]);
                        trapezio[5, 1, i].AddLine(vertici[5, 1], vertici1[5, 1]);
                    }

                    if (i == 1)
                    {
                        perimetri1[5, 1, i].AddLine(vertici[5, 0], vertici1[5, 1]);
                        perimetri1[5, 1, i].AddLine(vertici1[5, 1], vertici[0, 0]);
                        perimetri1[5, 1, i].AddLine(vertici[0, 0], vertici[5, 0]);
                        trapezio[5, 1, i].AddLine(vertici[0, 0], vertici[5, 0]);
                    }

                    if (i == 2)
                    {
                        perimetri1[5, 1, i].AddLine(vertici[0, 0], vertici1[5, 1]);
                        perimetri1[5, 1, i].AddLine(vertici1[5, 1], vertici[0, 1]);
                        perimetri1[5, 1, i].AddLine(vertici[0, 1], vertici[0, 0]);
                        trapezio[5, 1, i].AddLine(vertici1[5, 1], vertici[0, 1]);
                        trapezio[5, 1, i].AddLine(vertici[0, 1], vertici[0, 0]);
                    }
                }
            }
            #endregion

            #region Creazione cinque triangolini a settore traccia 2
            if (numTracce >= 3)
            {
                for (int i = 0; i < 5; i++)
                {
                    //BBB
                    perimetri2[0, 2, i] = new GraphicsPath();  // per creare i triangolini      //DA PROBLEMI CON LA LETTURA DELLA TASTIERA... dovevo metter if( numTracce >=3) altrimenti la leggeva anche con 2 tracce ma in questo caso i 5 triangolini non saranno visualizz mai -> nn li creo
                    trapezio[0, 2, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri2[0, 2, i].AddLine(vertici[0, 1], vertici[0, 2]);
                        perimetri2[0, 2, i].AddLine(vertici[0, 2], vertici2[0, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[0, 2], vertici[0, 1]);
                        trapezio[0, 2, i].AddLine(vertici[0, 1], vertici[0, 2]);
                        trapezio[0, 2, i].AddLine(vertici[0, 2], vertici2[0, 2]);
                    }

                    if (i == 1)
                    {
                        perimetri2[0, 2, i].AddLine(vertici[0, 1], vertici2[0, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[0, 2], vertici1[0, 1]);
                        perimetri2[0, 2, i].AddLine(vertici1[0, 1], vertici[0, 1]);
                        trapezio[0, 2, i].AddLine(vertici1[0, 1], vertici[0, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[0, 2, i].AddLine(vertici1[0, 1], vertici2[0, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[0, 2], vertici2[1, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[1, 2], vertici1[0, 1]);
                        trapezio[0, 2, i].AddLine(vertici2[0, 2], vertici2[1, 2]);
                    }

                    if (i == 3)
                    {
                        perimetri2[0, 2, i].AddLine(vertici1[0, 1], vertici2[1, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[1, 2], vertici[1, 1]);
                        perimetri2[0, 2, i].AddLine(vertici[1, 1], vertici1[0, 1]);
                        trapezio[0, 2, i].AddLine(vertici[1, 1], vertici1[0, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[0, 2, i].AddLine(vertici[1, 1], vertici2[1, 2]);
                        perimetri2[0, 2, i].AddLine(vertici2[1, 2], vertici[1, 2]);
                        perimetri2[0, 2, i].AddLine(vertici[1, 2], vertici[1, 1]);
                        trapezio[0, 2, i].AddLine(vertici2[1, 2], vertici[1, 2]);
                        trapezio[0, 2, i].AddLine(vertici[1, 2], vertici[1, 1]);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    perimetri2[1, 2, i] = new GraphicsPath();  // per creare i triangolini
                    trapezio[1, 2, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri2[1, 2, i].AddLine(vertici[1, 1], vertici[1, 2]);
                        perimetri2[1, 2, i].AddLine(vertici[1, 2], vertici2[2, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[2, 2], vertici[1, 1]);
                        trapezio[1, 2, i].AddLine(vertici[1, 1], vertici[1, 2]);
                        trapezio[1, 2, i].AddLine(vertici[1, 2], vertici2[2, 2]);
                    }

                    if (i == 1)
                    {
                        perimetri2[1, 2, i].AddLine(vertici[1, 1], vertici2[2, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[2, 2], vertici1[1, 1]);
                        perimetri2[1, 2, i].AddLine(vertici1[1, 1], vertici[1, 1]);
                        trapezio[1, 2, i].AddLine(vertici1[1, 1], vertici[1, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[1, 2, i].AddLine(vertici1[1, 1], vertici2[2, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[2, 2], vertici2[3, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[3, 2], vertici1[1, 1]);
                        trapezio[1, 2, i].AddLine(vertici2[2, 2], vertici2[3, 2]);
                    }

                    if (i == 3)
                    {
                        perimetri2[1, 2, i].AddLine(vertici1[1, 1], vertici2[3, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[3, 2], vertici[2, 1]);
                        perimetri2[1, 2, i].AddLine(vertici[2, 1], vertici1[1, 1]);
                        trapezio[1, 2, i].AddLine(vertici[2, 1], vertici1[1, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[1, 2, i].AddLine(vertici[2, 1], vertici2[3, 2]);
                        perimetri2[1, 2, i].AddLine(vertici2[3, 2], vertici[2, 2]);
                        perimetri2[1, 2, i].AddLine(vertici[2, 2], vertici[2, 1]);
                        trapezio[1, 2, i].AddLine(vertici2[3, 2], vertici[2, 2]);
                        trapezio[1, 2, i].AddLine(vertici[2, 2], vertici[2, 1]);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    perimetri2[2, 2, i] = new GraphicsPath();  // per creare i triangolini
                    trapezio[2, 2, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri2[2, 2, i].AddLine(vertici[2, 1], vertici[2, 2]);
                        perimetri2[2, 2, i].AddLine(vertici[2, 2], vertici2[4, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[4, 2], vertici[2, 1]);
                        trapezio[2, 2, i].AddLine(vertici[2, 1], vertici[2, 2]);
                        trapezio[2, 2, i].AddLine(vertici[2, 2], vertici2[4, 2]);
                    }

                    if (i == 1)
                    {
                        perimetri2[2, 2, i].AddLine(vertici[2, 1], vertici2[4, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[4, 2], vertici1[2, 1]);
                        perimetri2[2, 2, i].AddLine(vertici1[2, 1], vertici[2, 1]);
                        trapezio[2, 2, i].AddLine(vertici1[2, 1], vertici[2, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[2, 2, i].AddLine(vertici1[2, 1], vertici2[4, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[4, 2], vertici2[5, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[5, 2], vertici1[2, 1]);
                        trapezio[2, 2, i].AddLine(vertici2[4, 2], vertici2[5, 2]);
                    }

                    if (i == 3)
                    {
                        perimetri2[2, 2, i].AddLine(vertici1[2, 1], vertici2[5, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[5, 2], vertici[3, 1]);
                        perimetri2[2, 2, i].AddLine(vertici[3, 1], vertici1[2, 1]);
                        trapezio[2, 2, i].AddLine(vertici[3, 1], vertici1[2, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[2, 2, i].AddLine(vertici[3, 1], vertici2[5, 2]);
                        perimetri2[2, 2, i].AddLine(vertici2[5, 2], vertici[3, 2]);
                        perimetri2[2, 2, i].AddLine(vertici[3, 2], vertici[3, 1]);
                        trapezio[2, 2, i].AddLine(vertici2[5, 2], vertici[3, 2]);
                        trapezio[2, 2, i].AddLine(vertici[3, 2], vertici[3, 1]);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    perimetri2[3, 2, i] = new GraphicsPath();  // per creare i triangolini
                    trapezio[3, 2, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri2[3, 2, i].AddLine(vertici[3, 1], vertici[3, 2]);
                        perimetri2[3, 2, i].AddLine(vertici[3, 2], vertici2[6, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[6, 2], vertici[3, 1]);
                        trapezio[3, 2, i].AddLine(vertici[3, 1], vertici[3, 2]);
                        trapezio[3, 2, i].AddLine(vertici[3, 2], vertici2[6, 2]);
                    }

                    if (i == 1)
                    {
                        perimetri2[3, 2, i].AddLine(vertici[3, 1], vertici2[6, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[6, 2], vertici1[3, 1]);
                        perimetri2[3, 2, i].AddLine(vertici1[3, 1], vertici[3, 1]);
                        trapezio[3, 2, i].AddLine(vertici1[3, 1], vertici[3, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[3, 2, i].AddLine(vertici1[3, 1], vertici2[6, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[6, 2], vertici2[7, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[7, 2], vertici1[3, 1]);
                        trapezio[3, 2, i].AddLine(vertici2[6, 2], vertici2[7, 2]);
                    }

                    if (i == 3)
                    {
                        perimetri2[3, 2, i].AddLine(vertici1[3, 1], vertici2[7, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[7, 2], vertici[4, 1]);
                        perimetri2[3, 2, i].AddLine(vertici[4, 1], vertici1[3, 1]);
                        trapezio[3, 2, i].AddLine(vertici[4, 1], vertici1[3, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[3, 2, i].AddLine(vertici[4, 1], vertici2[7, 2]);
                        perimetri2[3, 2, i].AddLine(vertici2[7, 2], vertici[4, 2]);
                        perimetri2[3, 2, i].AddLine(vertici[4, 2], vertici[4, 1]);
                        trapezio[3, 2, i].AddLine(vertici2[7, 2], vertici[4, 2]);
                        trapezio[3, 2, i].AddLine(vertici[4, 2], vertici[4, 1]);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    perimetri2[4, 2, i] = new GraphicsPath();  // per creare i triangolini
                    trapezio[4, 2, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri2[4, 2, i].AddLine(vertici[4, 1], vertici[4, 2]);
                        perimetri2[4, 2, i].AddLine(vertici[4, 2], vertici2[8, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[8, 2], vertici[4, 1]);
                        trapezio[4, 2, i].AddLine(vertici[4, 1], vertici[4, 2]);
                        trapezio[4, 2, i].AddLine(vertici[4, 2], vertici2[8, 2]);
                    }

                    if (i == 1)
                    {
                        perimetri2[4, 2, i].AddLine(vertici[4, 1], vertici2[8, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[8, 2], vertici1[4, 1]);
                        perimetri2[4, 2, i].AddLine(vertici1[4, 1], vertici[4, 1]);
                        trapezio[4, 2, i].AddLine(vertici1[4, 1], vertici[4, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[4, 2, i].AddLine(vertici1[4, 1], vertici2[8, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[8, 2], vertici2[9, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[9, 2], vertici1[4, 1]);
                        trapezio[4, 2, i].AddLine(vertici2[8, 2], vertici2[9, 2]);
                    }

                    if (i == 3)
                    {
                        perimetri2[4, 2, i].AddLine(vertici1[4, 1], vertici2[9, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[9, 2], vertici[5, 1]);
                        perimetri2[4, 2, i].AddLine(vertici[5, 1], vertici1[4, 1]);
                        trapezio[4, 2, i].AddLine(vertici[5, 1], vertici1[4, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[4, 2, i].AddLine(vertici[5, 1], vertici2[9, 2]);
                        perimetri2[4, 2, i].AddLine(vertici2[9, 2], vertici[5, 2]);
                        perimetri2[4, 2, i].AddLine(vertici[5, 2], vertici[5, 1]);
                        trapezio[4, 2, i].AddLine(vertici2[9, 2], vertici[5, 2]);
                        trapezio[4, 2, i].AddLine(vertici[5, 2], vertici[5, 1]);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    perimetri2[5, 2, i] = new GraphicsPath();  // per creare i triangolini
                    trapezio[5, 2, i] = new GraphicsPath();

                    if (i == 0)
                    {
                        perimetri2[5, 2, i].AddLine(vertici[5, 1], vertici[5, 2]);
                        perimetri2[5, 2, i].AddLine(vertici[5, 2], vertici2[10, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[10, 2], vertici[5, 1]);
                        trapezio[5, 2, i].AddLine(vertici[5, 1], vertici[5, 2]);
                        trapezio[5, 2, i].AddLine(vertici[5, 2], vertici2[10, 2]);
                    }

                    if (i == 1)
                    {
                        perimetri2[5, 2, i].AddLine(vertici[5, 1], vertici2[10, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[10, 2], vertici1[5, 1]);
                        perimetri2[5, 2, i].AddLine(vertici1[5, 1], vertici[5, 1]);
                        trapezio[5, 2, i].AddLine(vertici1[5, 1], vertici[5, 1]);
                    }

                    if (i == 2)
                    {
                        perimetri2[5, 2, i].AddLine(vertici1[5, 1], vertici2[10, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[10, 2], vertici2[11, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[11, 2], vertici1[5, 1]);
                        trapezio[5, 2, i].AddLine(vertici2[10, 2], vertici2[11, 2]);
                    }

                    if (i == 3)
                    {
                        perimetri2[5, 2, i].AddLine(vertici1[5, 1], vertici2[11, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[11, 2], vertici[0, 1]);
                        perimetri2[5, 2, i].AddLine(vertici[0, 1], vertici1[5, 1]);
                        trapezio[5, 2, i].AddLine(vertici[0, 1], vertici1[5, 1]);
                    }

                    if (i == 4)
                    {
                        perimetri2[5, 2, i].AddLine(vertici[0, 1], vertici2[11, 2]);
                        perimetri2[5, 2, i].AddLine(vertici2[11, 2], vertici[0, 2]);
                        perimetri2[5, 2, i].AddLine(vertici[0, 2], vertici[0, 1]);
                        trapezio[5, 2, i].AddLine(vertici2[11, 2], vertici[0, 2]);
                        trapezio[5, 2, i].AddLine(vertici[0, 2], vertici[0, 1]);
                    }
                }
            }
            #endregion


            Centro = new Point(xCentro, yCentro);   // il centro di tutti gli esagoni (essendo concentrici è lo stesso)
            settore = 0;
            traccia = 0;

            settore1 = new GraphicsPath();      // li mettiamo GraphicsPath così poi potremo vedere se il ptatore si trova all'interno di uno di loro
            settore2 = new GraphicsPath();
            settore3 = new GraphicsPath();
            settore4 = new GraphicsPath();
            settore5 = new GraphicsPath();
            settore6 = new GraphicsPath();

            settore1.AddLine(Centro, vertici[0, numTracce - 1]);     // unisce il centro con il vertice dell'esagono + esterno
            settore1.AddLine(vertici[0, numTracce - 1], vertici[1, numTracce - 1]);
            settore1.AddLine(vertici[1, numTracce - 1], Centro);

            settore2.AddLine(Centro, vertici[1, numTracce - 1]);     // crea degli spicchi ke corrispondono ai vari settori (v foglio)
            settore2.AddLine(vertici[1, numTracce - 1], vertici[2, numTracce - 1]);
            settore2.AddLine(vertici[2, numTracce - 1], Centro);

            settore3.AddLine(Centro, vertici[2, numTracce - 1]);
            settore3.AddLine(vertici[2, numTracce - 1], vertici[3, numTracce - 1]);
            settore3.AddLine(vertici[3, numTracce - 1], Centro);

            //   1 2 
            //  0   3
            //   5 4

            settore4.AddLine(Centro, vertici[3, numTracce - 1]);
            settore4.AddLine(vertici[3, numTracce - 1], vertici[4, numTracce - 1]);
            settore4.AddLine(vertici[4, numTracce - 1], Centro);

            settore5.AddLine(Centro, vertici[4, numTracce - 1]);
            settore5.AddLine(vertici[4, numTracce - 1], vertici[5, numTracce - 1]);
            settore5.AddLine(vertici[5, numTracce - 1], Centro);

            settore6.AddLine(Centro, vertici[5, numTracce - 1]);
            settore6.AddLine(vertici[5, numTracce - 1], vertici[0, numTracce - 1]);
            settore6.AddLine(vertici[0, numTracce - 1], Centro);

        }

        //funziona utilizzata per disegnare il tasto quando si solleva l'evento Paint
        public void disegna(Graphics dove, Color inattivo, Color temporaneo, Color attivo, Color trasparente, SolidBrush colorcontornobott) //sbolly
        {
            switch (stato)
            {
                case Stato.Inattivo:
                    if (Shift.tastoAttivo == 0 && Lock.tastoAttivo == 0)
                    {
                        dimensioneTesto = dove.MeasureString(testo[mode, 0][0], f);
                        dove.FillPath(new SolidBrush(inattivo), perimetri[0]);      // perimetri [0], devi colorare il perimetro dell'esagono + interno 
                        posizioneTesto.X = xCentro - (dimensioneTesto.Width / 2) + 0.5f;
                        posizioneTesto.Y = yCentro - (dimensioneTesto.Height / 2);
                        dove.DrawString(testo[mode, settore][triangolo], f, new SolidBrush(coloreFont), posizioneTesto);
                    }
                    else
                    {
                        if (Shift.tastoAttivo == 1 && Lock.tastoAttivo == 0)
                        {
                            // Il primo carattere se lo shift è attivo è maiuscolo, gli altri no
                            string testoShift = char.ToUpper(testo[mode, settore][triangolo][0]) + testo[mode, settore][triangolo].Substring(1, testo[mode, settore][triangolo].Length - 1);
                            dimensioneTesto = dove.MeasureString(testoShift, f);
                            dove.FillPath(new SolidBrush(inattivo), perimetri[0]);      // perimetri [0], devi colorare il perimetro dell'esagono + interno 
                            posizioneTesto.X = xCentro - (dimensioneTesto.Width / 2) + 0.5f;
                            posizioneTesto.Y = yCentro - (dimensioneTesto.Height / 2);
                            dove.DrawString(testoShift, f, new SolidBrush(coloreFont), posizioneTesto);
                        }
                        if (Lock.tastoAttivo == 1)
                        {
                            // con lock attivo tutti i caratteri sono resi maiuscoli
                            string testoLock = testo[mode, settore][triangolo].ToUpper();
                            dimensioneTesto = dove.MeasureString(testoLock, f);
                            dove.FillPath(new SolidBrush(inattivo), perimetri[0]);      // perimetri [0], devi colorare il perimetro dell'esagono + interno 
                            posizioneTesto.X = xCentro - (dimensioneTesto.Width / 2) + 0.5f;
                            posizioneTesto.Y = yCentro - (dimensioneTesto.Height / 2);
                            dove.DrawString(testoLock, f, new SolidBrush(coloreFont), posizioneTesto);
                        }
                    }
                    
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetri[0]);
                    break;
                case Stato.Attivo:
                    Cursor.Current = Form1.getCustomCursor();
                    dove.FillPath(new SolidBrush(attivo), perimetri[0]);
		            posizioneTesto.Y -= Glob.poslettere;
                    if (Shift.tastoAttivo == 0 && Lock.tastoAttivo == 0)
                        dove.DrawString(testo[mode,settore][triangolo], f, new SolidBrush(Color.FromName(coloreTestoMobile)), posizioneTesto);
                    else
                    {
                        if (Shift.tastoAttivo == 1 && Lock.tastoAttivo == 0)
                        {
                            string testoShift;
                            if (testo[mode, settore][triangolo].Length == 1)
                                testoShift = char.ToUpper(testo[mode, settore][triangolo][0]).ToString();
                            else
                                testoShift = char.ToUpper(testo[mode, settore][triangolo][0]).ToString() + testo[mode, settore][triangolo][1].ToString();
                            dove.DrawString(testoShift, f, new SolidBrush(Color.FromName(coloreTestoMobile)), posizioneTesto);
                        }
                        if (Lock.tastoAttivo == 1)
                        {
                            string testoLock = testo[mode, settore][triangolo].ToUpper();
                            dove.DrawString(testoLock, f, new SolidBrush(Color.FromName(coloreTestoMobile)), posizioneTesto);
                        }
                    }
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetri[0]);

                    for (int j = 0; j < 6; j++)     // le istruzioni in questo for e in quello dopo nn vengono mai eseguite!!
                    {
                        if ((j == newSettore) && (newTraccia == 1))
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                //dove.FillPath(new SolidBrush(inattivo), perimetri1[j, 1, i]);
                                dove.DrawPath(new Pen(Brushes.LightBlue, 1), perimetri1[j, 1, i]);
                                if (Shift.tastoAttivo == 0 && Lock.tastoAttivo == 0)
                                    dove.DrawString(testo[mode,settore][triangolo], f, new SolidBrush(coloreFont), posizioneTesto);
                                else
                                {
                                    if (Shift.tastoAttivo == 1 && Lock.tastoAttivo == 0)
                                    {
                                        string testoShift = char.ToUpper(testo[mode,settore][triangolo][0]) + testo[mode,settore][triangolo].Substring(1, testo[mode,settore][triangolo].Length - 1);
                                        dove.DrawString(testoShift, f, new SolidBrush(coloreFont), posizioneTesto);
                                    }
                                    if (Lock.tastoAttivo == 1)
                                    {
                                        string testoLock = testo[mode,settore][triangolo].ToUpper();
                                        dove.DrawString(testoLock, f, new SolidBrush(coloreFont), posizioneTesto);
                                    }
                                }
                            }

                            if (j == 0)       // ho tentato di far comparire il bezier sopra i triangolini
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[0, 0], posizioneCorrente, posizioneCorrente, vertici[1, 0]);
                                
                                posizioneCorrente.X = posizioneCorrente.X - 1;    //PROVA
                                
                                perimetri[0].AddLine(vertici[0, 0], posizioneCorrente);     
                                perimetri[0].AddLine(posizioneCorrente, vertici[1, 0]);
                            }
                            if (j == 1)
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[1, 0], posizioneCorrente, posizioneCorrente, vertici[2, 0]);

                                perimetri[0].AddLine(vertici[1, 0], posizioneCorrente);     
                                perimetri[0].AddLine(posizioneCorrente, vertici[2, 0]);
                            }
                            if (j == 2)
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[2, 0], posizioneCorrente, posizioneCorrente, vertici[3, 0]);

                                perimetri[0].AddLine(vertici[2, 0], posizioneCorrente);
                                perimetri[0].AddLine(posizioneCorrente, vertici[3, 0]);
                            }
                            if (j == 3)
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[3, 0], posizioneCorrente, posizioneCorrente, vertici[4, 0]);

                                perimetri[0].AddLine(vertici[3, 0], posizioneCorrente);
                                perimetri[0].AddLine(posizioneCorrente, vertici[4, 0]);
                            }
                            if (j == 4)
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[4, 0], posizioneCorrente, posizioneCorrente, vertici[5, 0]);

                                perimetri[0].AddLine(vertici[4, 0], posizioneCorrente);
                                perimetri[0].AddLine(posizioneCorrente, vertici[5, 0]);
                            }
                            if (j == 5)
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[5, 0], posizioneCorrente, posizioneCorrente, vertici[0, 0]);

                                perimetri[0].AddLine(vertici[5, 0], posizioneCorrente);
                                perimetri[0].AddLine(posizioneCorrente, vertici[0, 0]);
                            }
                        }

                        
                    }
                    for (int j = 0; j < 6; j++)
                    {
                        if ((newSettore == j) && (newTraccia == 2))
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                //dove.FillPath(new SolidBrush(inattivo), perimetri1[j, 1, i]);
                                dove.DrawPath(new Pen(Brushes.LightBlue, 1), perimetri1[j, 1, i]);
                                if (Shift.tastoAttivo == 0 && Lock.tastoAttivo == 0)
                                    dove.DrawString(testo[mode,settore][triangolo], f, new SolidBrush(coloreFont), posizioneTesto);
                                else
                                {
                                    if (Shift.tastoAttivo == 1 && Lock.tastoAttivo == 0)
                                    {
                                        string testoShift = char.ToUpper(testo[mode,settore][triangolo][0]) + testo[mode,settore][triangolo].Substring(1, testo[mode,settore][triangolo].Length - 1);
                                        dove.DrawString(testoShift, f, new SolidBrush(coloreFont), posizioneTesto);
                                    }
                                    if (Lock.tastoAttivo == 1)
                                    {
                                        string testoLock = testo[mode,settore][triangolo].ToUpper();
                                        dove.DrawString(testoLock, f, new SolidBrush(coloreFont), posizioneTesto);
                                    }
                                }
                            }

                            for (int i = 0; i < 5; i++)
                            {
                                //dove.FillPath(new SolidBrush(inattivo), perimetri2[j, 2, i]);
                                dove.DrawPath(new Pen(Brushes.LightBlue, 1), perimetri2[j, 2, i]);
                                if (Shift.tastoAttivo == 0 && Lock.tastoAttivo == 0)
                                    dove.DrawString(testo[mode,settore][triangolo], f, new SolidBrush(coloreFont), posizioneTesto);
                                else
                                {
                                    if (Shift.tastoAttivo == 1 && Lock.tastoAttivo == 0)
                                    {
                                        string testoShift = char.ToUpper(testo[mode,settore][triangolo][0]) + testo[mode,settore][triangolo].Substring(1, testo[mode,settore][triangolo].Length - 1);
                                        dove.DrawString(testoShift, f, new SolidBrush(coloreFont), posizioneTesto);
                                    }
                                    if (Lock.tastoAttivo == 1)
                                    {
                                        string testoLock = testo[mode,settore][triangolo].ToUpper();
                                        dove.DrawString(testoLock, f, new SolidBrush(coloreFont), posizioneTesto);
                                    }
                                }
                            }

                            if (j == 0)       // ho tentato di far comparire il bezier sopra i triangolini
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[0, 0], posizioneCorrente, posizioneCorrente, vertici[1, 0]);

                                perimetri[0].AddLine(vertici[0, 0], posizioneCorrente);
                                perimetri[0].AddLine(posizioneCorrente, vertici[1, 0]);
                            }
                            if (j == 1)
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[1, 0], posizioneCorrente, posizioneCorrente, vertici[2, 0]);

                                perimetri[0].AddLine(vertici[1, 0], posizioneCorrente);
                                perimetri[0].AddLine(posizioneCorrente, vertici[2, 0]);
                            }
                            if (j == 2)
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[2, 0], posizioneCorrente, posizioneCorrente, vertici[3, 0]);

                                perimetri[0].AddLine(vertici[2, 0], posizioneCorrente);
                                perimetri[0].AddLine(posizioneCorrente, vertici[3, 0]);
                            }
                            if (j == 3)
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[3, 0], posizioneCorrente, posizioneCorrente, vertici[4, 0]);

                                perimetri[0].AddLine(vertici[3, 0], posizioneCorrente);
                                perimetri[0].AddLine(posizioneCorrente, vertici[4, 0]);
                            }
                            if (j == 4)
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[4, 0], posizioneCorrente, posizioneCorrente, vertici[5, 0]);

                                perimetri[0].AddLine(vertici[4, 0], posizioneCorrente);
                                perimetri[0].AddLine(posizioneCorrente, vertici[5, 0]);
                            }
                            if (j == 5)
                            {
                                dove.DrawPath(new Pen(Brushes.Transparent, 2), perimetri[0]);
                                //perimetri[0].AddBezier(vertici[5, 0], posizioneCorrente, posizioneCorrente, vertici[0, 0]);

                                perimetri[0].AddLine(vertici[5, 0], posizioneCorrente);
                                perimetri[0].AddLine(posizioneCorrente, vertici[0, 0]);
                            }
                        }
                    }
                    
                    break;
                case Stato.Trasparente:
                    dove.FillPath(new SolidBrush(trasparente), perimetri[0]);
                    dove.DrawString(testo[mode,settore][triangolo], f, new SolidBrush(trasparente), posizioneTesto);
                    dove.DrawPath(new Pen(new SolidBrush(trasparente), 2), perimetri[0]); 
                    dove.DrawPath(new Pen(colorcontornobott, 1), perimetri[0]); 
                    break;
                case Stato.Temporaneo:
                    dove.FillPath(new SolidBrush(temporaneo), perimetri[0]);
                    if (Shift.tastoAttivo == 0 && Lock.tastoAttivo == 0)
                        dove.DrawString(testo[mode,0][0], f, new SolidBrush(coloreFont), posizioneTesto);     
                    else
                    {
                        if (Shift.tastoAttivo == 1 && Lock.tastoAttivo == 0)
                        {
                            string testoShift = char.ToUpper(testo[mode,0][0][0]) + testo[mode,0][0].Substring(1, testo[mode,0][0].Length - 1);
                            dove.DrawString(testoShift, f, new SolidBrush(coloreFont), posizioneTesto);
                        }
                        if (Lock.tastoAttivo == 1)
                        {
                            string testoLock = testo[mode,0][0].ToUpper();
                            dove.DrawString(testoLock, f, new SolidBrush(coloreFont), posizioneTesto);
                        }
                    }
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetri[0]); 
                    break;
            }
            dimensioneTesto = dove.MeasureString(testo[mode,settore][triangolo], f);                       
        }



        //funzione per rilevare la traccia in cui si trova il puntatore     (metodo di ausilio per det dov'è il puntatore)
        public int GetTraccia(Point posizione)      //gli passi la posiz del puntatore (di classe point ke contiene 2 variabili: la x e la y del ptatore)
        {
            // sottointeso i tasti esag attivi infatti è una var statica e nelle altre classi specifica tastiEsag.tastiAttivi
            
            /*if (stato == Stato.Inattivo && tastiAttivi == 1)    // se lo stato è inattivo ma cè un tasto attivo diventa trasparente
                stato = Stato.Trasparente;*/

            if (stato == Stato.Trasparente && (tastiAttivi == 0 && TastiRotondi.tastiAttivi==0 && Space.tastiAttivi==0 && BackSpace.tastoAttivo==0 ))
                stato = Stato.Inattivo;     // se lo stato è trasparente e tutti gli altri tasti sono inattivi --> non può esser trasparente --> è inattivo
            if (perimetri[0].IsVisible(posizione))      // se nel perimetro dell esagono + interno cè il ptatore
            {
                if (tastiTemporanei == 0 && stato == Stato.Inattivo)    // se nn ci sono altri tasti temporanei e se stato era inattivo --> temporaneo (come in qll rotondi)
                {
                    stato = Stato.Temporaneo;   
                    tastiTemporanei = 1;        // è in qst classe ma è privata e statica
                }
                return 0;   // in ogni caso se è sopra all'area dell'esagono + interno torni 0 ossia la traccia 0
            }
            else               // se ptatore nn è sul tasto (come nei tasti rotondi)
            {
                if (stato == Stato.Temporaneo)  //se è temporaneo passo a inattivo
                {
                    stato = Stato.Inattivo;
                    tastiTemporanei = 0;
                }

                if (stato == Stato.Attivo)      //"IF" DI PROVA  --- E' SERVITO --- 
                {
                    for (int i = 1; i < numTracce; i++)     // controlla se il ptatore è nelle tracce sucessive dal perimetro 1 (ossia dall'esagono dal secondo esagono partendo dal + interno dato ke il + interno l'abbiamo gia controllato) al + esterno
                    {
                        if (perimetri[i].IsVisible(posizione))  // se cè ritorna la traccia sopra cui è
                            return i;
                    }
                }
            }
            if (stato == Stato.Attivo)   // come nei tasti rotondi se è attivo passi a inattivo, MA non dovrebbe esser dentro l'else come nei tasti rotondi??
            {
                tastiAttivi = 0;
                stato = Stato.Inattivo;
            }
            return triangolo;
        }


        /*funzione privata per capire il settore in cui si trava il puntatore rispetto al reticolato 
         * del tasto che viene chiamata ad ogni movimento*/
        public int GetSettore(Point posizione)
        {
            if (settore1.IsVisible(posizione))      //se è visibile nel settore 1 torni 0...
            {
                return 0;
            }
            else if (settore2.IsVisible(posizione)) //...se è visibile nel settore 2 torni 1... e così via per tutti e 6 i settori
            {
                return 1;
            }
            else if (settore3.IsVisible(posizione))
            {
                return 2;
            }
            else if (settore4.IsVisible(posizione))
            {
                return 3;
            }
            else if (settore5.IsVisible(posizione))
            {
                return 4;
            }
            else if (settore6.IsVisible(posizione))
            {
                return 5;
            }
            else
                return -1;      // il ptatore non è sul tasto
        }

        
        public int getTriangolo (Point posizione)
        {
            if (newTraccia == 0)
            {
                return 0;
            }
            
            //else if (newTraccia == 1)               // vedo in che traccia sono... LI HO TOLTI SIA QUESTO KE QUELLO DOPO, MI PROVOCAVANO 1 ECCEZ NELL ASSEGNARE UNA PSEUDOSILLABA A OGNI TRIANGOLINO
            //{
                for (int j = 0; j < 6; j++)    //...poi guardo il settore...
                {
                    for (int i = 0; i < 3; i++)  //... e per ogni settore guardo il triangolino su cui può trovarsi il mouse...
                    {
                        if (perimetri1[j, 1, i].IsVisible(posizione))
                            return i+1;           // se lo trovo ritorno il numero del triangolino
                    }
                }
            //}

            //else if (newTraccia == 2)
            //{
                for (int j = 0; j < 6; j++)
                {
                    for (int i = 0; i < 5; i++)     // controlla se il ptatore è nelle tracce sucessive dal perimetro 1 (ossia dall'esagono dal secondo esagono partendo dal + interno dato ke il + interno l'abbiamo gia controllato) al + esterno
                    {
                        if (perimetri2[j, 2, i].IsVisible(posizione))  // se cè ritorna la traccia sopra cui è
                            return i+4;
                    }
                }
            //}

            //else      //non ce n è bisogno perchè se arriva a leggere il -1 vuol dire che nn ci troviamo in nessuna traccia quindi è errore
            return 0;//ddbb

        }
        //funzione per modificare lo stato del tasto in base alla posizione del puntatore
        // e responsabile del calcolo del nuovo perimetro con il lato deformato
        public void modificaTasto(int xPosizione, int yPosizione)
        {
            posizioneCorrente = new Point(xPosizione, yPosizione);  // la posiz corr del ptatore
            
            newTraccia = this.GetTraccia(posizioneCorrente);        // attraverso i metodi di ausilio guardo dove in ke traccia e settore di trova il ptatore x definire 1 area ben precisa
            newSettore = this.GetSettore(posizioneCorrente);           
            newTriangolo = this.getTriangolo(posizioneCorrente);//ddbb

            if (newSettore >= 0 && newTraccia >=0 && stato == Stato.Attivo)
            {
                Cursor.Current = Form1.getCustomCursor();

                if (perimetri[0] != null)       //se perimetro dell esagono + interno è diverso da null lo elimini tu
                    perimetri[0].Dispose();     //rilasci le risorse prima che il garbage collector se ne occupi, magari è inutile portarci dietro sta var, infatti in sto caso se sia il settore ke la traccia sono >0 il perimetro dell esagono + interno non ci serve!

                perimetri[0] = new GraphicsPath();
                
                if (newTraccia == 0)    // se si trova nella prima traccia --> prende tutto l'esagono + interno --> lo ricostruisci bene
                {
                    perimetri[0].AddLine(vertici[0, 0], vertici[1, 0]);
                    perimetri[0].AddLine(vertici[1, 0], vertici[2, 0]);
                    perimetri[0].AddLine(vertici[2, 0], vertici[3, 0]);
                    perimetri[0].AddLine(vertici[3, 0], vertici[4, 0]);
                    perimetri[0].AddLine(vertici[4, 0], vertici[5, 0]);
                    perimetri[0].AddLine(vertici[5, 0], vertici[0, 0]);
                }
                else     // se il newTraccia != 0 controlli il settore
                {
                    Point[] pSett0 = { vertici[0, 0], posizioneCorrente,posizioneCorrente, vertici[1, 0] };       // per usare AddLines(point[])
                    Point[] pSett1 = { vertici[1, 0], posizioneCorrente,posizioneCorrente, vertici[2, 0] };
                    Point[] pSett2 = { vertici[2, 0], posizioneCorrente,posizioneCorrente, vertici[3, 0] };
                    Point[] pSett3 = { vertici[3, 0], posizioneCorrente,posizioneCorrente, vertici[4, 0] };
                    Point[] pSett4 = { vertici[4, 0], posizioneCorrente,posizioneCorrente, vertici[5, 0] };
                    Point[] pSett5 = { vertici[5, 0], posizioneCorrente,posizioneCorrente, vertici[0, 0] }; 
                    
                    switch (newSettore)
                    {                     
                        case 0:
                            //perimetri[0].AddBezier(vertici[0, 0], posizioneCorrente, posizioneCorrente, vertici[1, 0]);     // crei quella deformazione ke si muove come muovi il che unisce i 2 vertici alla posCorrente del ptatore
                            perimetri[0].AddLine(vertici[0, 0].X, vertici[0, 0].Y, posizioneCorrente.X + 1, posizioneCorrente.Y);     // al posto di bezier uso qst!!
                            perimetri[0].AddLine(posizioneCorrente.X + 1, posizioneCorrente.Y, vertici[1, 0].X, vertici[1, 0].Y);

                            //perimetri[0].AddLines(pSett0);
                            perimetri[0].AddLine(vertici[1, 0], vertici[2, 0]);
                            perimetri[0].AddLine(vertici[2, 0], vertici[3, 0]);
                            perimetri[0].AddLine(vertici[3, 0], vertici[4, 0]);
                            perimetri[0].AddLine(vertici[4, 0], vertici[5, 0]);
                            perimetri[0].AddLine(vertici[5, 0], vertici[0, 0]);
                            break;
                        case 1:
                            //perimetri[0].AddBezier(vertici[1, 0], posizioneCorrente, posizioneCorrente, vertici[2, 0]);

                            perimetri[0].AddLine(vertici[1, 0], posizioneCorrente);     // al posto di bezier uso qst!!
                            perimetri[0].AddLine(posizioneCorrente, vertici[2, 0]);

                            perimetri[0].AddLine(vertici[2, 0], vertici[3, 0]);
                            perimetri[0].AddLine(vertici[3, 0], vertici[4, 0]);
                            perimetri[0].AddLine(vertici[4, 0], vertici[5, 0]);
                            perimetri[0].AddLine(vertici[5, 0], vertici[0, 0]);
                            perimetri[0].AddLine(vertici[0, 0], vertici[1, 0]);
                            break;
                        case 2:
                            //perimetri[0].AddBezier(vertici[2, 0], posizioneCorrente, posizioneCorrente, vertici[3, 0]);

                            perimetri[0].AddLine(vertici[2, 0], posizioneCorrente);     // al posto di bezier uso qst!!
                            perimetri[0].AddLine(posizioneCorrente, vertici[3, 0]);

                            perimetri[0].AddLine(vertici[3, 0], vertici[4, 0]);
                            perimetri[0].AddLine(vertici[4, 0], vertici[5, 0]);
                            perimetri[0].AddLine(vertici[5, 0], vertici[0, 0]);
                            perimetri[0].AddLine(vertici[0, 0], vertici[1, 0]);
                            perimetri[0].AddLine(vertici[1, 0], vertici[2, 0]);
                            break;
                        case 3:
                            //perimetri[0].AddBezier(vertici[3, 0], posizioneCorrente, posizioneCorrente, vertici[4, 0]);

                            perimetri[0].AddLine(vertici[3, 0], posizioneCorrente);     // al posto di bezier uso qst!!
                            perimetri[0].AddLine(posizioneCorrente, vertici[4, 0]);

                            perimetri[0].AddLine(vertici[4, 0], vertici[5, 0]);
                            perimetri[0].AddLine(vertici[5, 0], vertici[0, 0]);
                            perimetri[0].AddLine(vertici[0, 0], vertici[1, 0]);
                            perimetri[0].AddLine(vertici[1, 0], vertici[2, 0]);
                            perimetri[0].AddLine(vertici[2, 0], vertici[3, 0]);
                            break;
                        case 4:
                            //perimetri[0].AddBezier(vertici[4, 0], posizioneCorrente, posizioneCorrente, vertici[5, 0]);

                            perimetri[0].AddLine(vertici[4, 0], posizioneCorrente);     // al posto di bezier uso qst!!
                            perimetri[0].AddLine(posizioneCorrente, vertici[5, 0]);

                            perimetri[0].AddLine(vertici[5, 0], vertici[0, 0]);
                            perimetri[0].AddLine(vertici[0, 0], vertici[1, 0]);
                            perimetri[0].AddLine(vertici[1, 0], vertici[2, 0]);
                            perimetri[0].AddLine(vertici[2, 0], vertici[3, 0]);
                            perimetri[0].AddLine(vertici[3, 0], vertici[4, 0]);
                            break;
                        case 5:
                            //perimetri[0].AddBezier(vertici[5, 0], posizioneCorrente, posizioneCorrente, vertici[0, 0]);

                            perimetri[0].AddLine(vertici[5, 0].X, vertici[5, 0].Y, posizioneCorrente.X + 1, posizioneCorrente.Y);     // al posto di bezier uso qst!!
                            perimetri[0].AddLine(posizioneCorrente.X + 1, posizioneCorrente.Y, vertici[0, 0].X, vertici[0, 0].Y);

                            perimetri[0].AddLine(vertici[0, 0], vertici[1, 0]);
                            perimetri[0].AddLine(vertici[1, 0], vertici[2, 0]);
                            perimetri[0].AddLine(vertici[2, 0], vertici[3, 0]);
                            perimetri[0].AddLine(vertici[3, 0], vertici[4, 0]);
                            perimetri[0].AddLine(vertici[4, 0], vertici[5, 0]);
                            break;
                    }
                }
                posizioneTesto.X = xPosizione - (dimensioneTesto.Width / 2) + 0.5f;
                posizioneTesto.Y = yPosizione - (dimensioneTesto.Height / 2);

                settore = newSettore;
                traccia = newTraccia;
                triangolo = newTriangolo;
            }
            else      // se è -1
            {
            
                if (perimetri[0] != null)   // rilasci le risorse ke non servono...
                    perimetri[0].Dispose();

                perimetri[0] = new GraphicsPath();  //... e ricrei l'esagono + interno
                perimetri[0].AddLine(vertici[0, 0], vertici[1, 0]);
                perimetri[0].AddLine(vertici[1, 0], vertici[2, 0]);
                perimetri[0].AddLine(vertici[2, 0], vertici[3, 0]);
                perimetri[0].AddLine(vertici[3, 0], vertici[4, 0]);
                perimetri[0].AddLine(vertici[4, 0], vertici[5, 0]);
                perimetri[0].AddLine(vertici[5, 0], vertici[0, 0]);
                settore = 0;
                traccia = 0;

                triangolo = 0;
            }           
        }

        //funzione di cambio stato
        public int controlloStato()
        {
            if (stato == Stato.Attivo)
            {
                Cursor.Current = Form1.getCustomCursor();
                return 1;
            }
            else
                return 0;
        }

        public void attivaTasto()
        {
            if (tastiAttivi == 0)
            {
                stato = Stato.Attivo;
                tastiAttivi = 1;
                tastiTemporanei = 0;
                posizioneTesto.X = posizioneCorrente.X - (dimensioneTesto.Width / 2) + 0.5f;
                posizioneTesto.Y = posizioneCorrente.Y - (dimensioneTesto.Height / 2);
            }
        }

        public string ritornaStringa(int settore, int triangolo)
        {
            return this.testo[mode,settore][triangolo];//ddbb         
        }

        public void disattiva()
        {
            tastiAttivi = 0;
            stato = Stato.Inattivo;
        }

        public void rendiTrasparente()
        {
            //stato = Stato.Trasparente;
        }

        public int getTracciaGiusta()   // PROVA, serve a cercar di aggiustare il probl sett 0-5  --- E' SERVITO ---
        {
            return traccia;
        }

        public int getSettoreGiusto()   // PROVA, serve a cercar di aggiustare il probl sett 0-5  
        {
            return settore;
        }
        
    }
}
