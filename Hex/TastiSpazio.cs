using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Hex
{
    public class Space
    {
        public int xCentro, yCentro;
        int deltaX, deltaY;
        public enum Stato { Attivo, Inattivo, Trasparente, Temporaneo }
        public Stato stato;
        public GraphicsPath perimetro;
        public Font f;
        Point Centro;
        Point vertice1,vertice2,vertice3,vertice4;
        public SizeF dimensioneTesto;
        public PointF posizioneTesto = new PointF();
        public static int tastiAttivi = 0;
        static int tastiTemporanei = 0;
        Point posizioneCorrente;
        public string testo;

        Color coloreFont = Form1.getColoreFont();

        public Space(int xCentro, int yCentro, int deltaX, int deltaY, Font f, string testo)
        {
            this.xCentro = xCentro;
            this.yCentro = yCentro;
            this.deltaX = deltaX;
            this.deltaY = deltaY;
            this.testo = testo;
            this.f = f;
            stato = Stato.Inattivo;
            perimetro = new GraphicsPath();
            vertice1 = new Point(xCentro - deltaX / 2, yCentro - deltaY / 2);
            vertice2 = new Point(xCentro + deltaX / 2, yCentro - deltaY / 2);
            vertice3 = new Point(xCentro + deltaX / 2, yCentro + deltaY / 2);
            vertice4 = new Point(xCentro - deltaX / 2, yCentro + deltaY / 2);
            perimetro.AddLine(vertice1, vertice2);
            perimetro.AddLine(vertice2, vertice3);
            perimetro.AddLine(vertice3, vertice4);
            perimetro.AddLine(vertice4, vertice1);
            Centro = new Point(xCentro, yCentro);
        }

        public void ModTastSpace(float dim,float dimcar)
        {
            this.xCentro = Convert.ToInt32(xCentro * dim);
            this.yCentro = Convert.ToInt32(yCentro*dim);
            this.deltaX = Convert.ToInt32(deltaX*dim);
            this.deltaY = Convert.ToInt32(deltaY*dim);
            f = new Font(FontFamily.GenericSansSerif, dimcar * dim);
            perimetro = new GraphicsPath();
            vertice1 = new Point(xCentro - deltaX / 2, yCentro - deltaY / 2);
            vertice2 = new Point(xCentro + deltaX / 2, yCentro - deltaY / 2);
            vertice3 = new Point(xCentro + deltaX / 2, yCentro + deltaY / 2);
            vertice4 = new Point(xCentro - deltaX / 2, yCentro + deltaY / 2);
            perimetro.AddLine(vertice1, vertice2);
            perimetro.AddLine(vertice2, vertice3);
            perimetro.AddLine(vertice3, vertice4);
            perimetro.AddLine(vertice4, vertice1);
            Centro = new Point(xCentro, yCentro);
        }

        //funziona utilizzata per disegnare il tasto quando si solleva l'evento Paint
        public void disegna(Graphics dove, Color inattivo, Color temporaneo, Color attivo, Color trasparente, SolidBrush colorcontornobott)
        {
            switch (stato)
            {
                case Stato.Inattivo:
                    dimensioneTesto = dove.MeasureString(testo, f);
                    dove.FillPath(new SolidBrush(inattivo), perimetro);
                    posizioneTesto.X = xCentro - (dimensioneTesto.Width / 2);
                    posizioneTesto.Y = yCentro - (dimensioneTesto.Height / 2);
                    dove.DrawString(testo, f, new SolidBrush(coloreFont), posizioneTesto);
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetro);
                    break;
                case Stato.Attivo:
                    Cursor.Current = Form1.getCustomCursor();

                    dove.FillPath(new SolidBrush(attivo), perimetro);
                    dove.DrawString(testo, f, new SolidBrush(coloreFont), posizioneTesto);
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetro);
                    break;
                case Stato.Trasparente:
                    dove.FillPath(new SolidBrush(trasparente), perimetro);
                    dove.DrawString(testo, f, new SolidBrush(trasparente), posizioneTesto);
                    dove.DrawPath(new Pen(new SolidBrush(trasparente), 2), perimetro);
                    dove.DrawPath(new Pen(colorcontornobott, 1), perimetro);
                    break;
                case Stato.Temporaneo:
                    dove.FillPath(new SolidBrush(temporaneo), perimetro);
                    dove.DrawString(testo, f, new SolidBrush(coloreFont), posizioneTesto);
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetro);
                    break;
            }  
        }



        //funzione per modificare lo stato del tasto in base alla posizione del puntatore
        public void modificaTasto(int xPosizione, int yPosizione)
        {
            posizioneCorrente = new Point(xPosizione, yPosizione);
            /*if (stato == Stato.Inattivo && TastiEsagonali.tastiAttivi == 1)
                ; //XYXYXY stato = Stato.Trasparente;*/
            if (stato == Stato.Trasparente && (tastiAttivi == 0 && TastiEsagonali.tastiAttivi == 0 && TastiRotondi.tastiAttivi==0 && BackSpace.tastoAttivo==0))
                stato = Stato.Inattivo;
            if (TastiEsagonali.tastiAttivi == 1)     // se il ptatore � nel tasto esagonale --> tasto rotondo � inattivo
                stato = Stato.Inattivo;
            else if (perimetro.IsVisible(posizioneCorrente))
            {
                if (tastiTemporanei == 0 && stato == Stato.Inattivo)
                {
                    stato = Stato.Temporaneo;
                    tastiTemporanei = 1;
                }
            }
            else
            {
                if (stato == Stato.Temporaneo)
                {
                    stato = Stato.Inattivo;
                    tastiTemporanei = 0;
                }

                if (stato == Stato.Attivo)
                {
                    tastiAttivi = 0;
                    stato = Stato.Inattivo;
                }
            }
        }



        //funzioni per cambiare lo stato del tasto
        public void rendiTrasparente()
        {
            ; //XYXYXY stato = Stato.Trasparente;
        }

        public void attivaTasto()
        {
            //Cursor.Current = Form1.getCustomCursor();

            stato = Stato.Attivo;
            tastiAttivi = 1;
            tastiTemporanei = 0;
        }
        public void disattiva()
        {
            tastiAttivi = 0;
            stato = Stato.Inattivo;
        }
    }
}
