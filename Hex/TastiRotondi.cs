using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Hex
{
    public class TastiRotondi
    {
        public int xCentro, yCentro;
        int raggio;
        public enum Stato { Attivo, Inattivo, Trasparente, Temporaneo }
        public Stato stato;                 // serve nello switch
        public GraphicsPath perimetro;
        public Font f;
        Point Centro;
        public SizeF dimensioneTesto;                // a che serve? non � gia definito in point? SERVE A SAPER DOVE METTERE IL TESTO NEL RETTANGOLO la calcoli passandogli f e la stringa del testo
        public PointF posizioneTesto= new PointF();     // posizione testo � formato da 2 valori: la x e la y in cui � centrato il carattere
        public string testo;            // credo corrisponda al carattere perch� il tipo di scrittura dovr esser definita in "Font" QUASI SICURO
        public string testoSL;
        Point posizioneCorrente;            // usato in "modifica tasto"
        public static int tastiAttivi = 0;
        public int tastiTemporanei = 0;

        Color coloreFont = Form1.getColoreFont();


        //costruttore utilizzato per formare il tasto           // per font intende la dimens il tipo carattere ecc (un metodo di system.drawing)
        public TastiRotondi(int xCentro, int yCentro, int raggio,Font f, string testo)
        {
            this.xCentro = xCentro;
            this.yCentro = yCentro;
            this.raggio = raggio;
            this.testo = testo;     // carattere o tipo di scrittura (es times new roman)???
            this.f = f;
            stato = Stato.Inattivo;
            testoSL = testo.ToUpper();  //??
            perimetro = new GraphicsPath();
            perimetro.AddEllipse(xCentro-raggio,yCentro-raggio,2*raggio,2*raggio);      //?? a perch� serve un ellisse? e i parametri?
            Centro = new Point(xCentro, yCentro);
        }

        public void ModTastRot(float dim,float dimcar)
        {
            this.xCentro =Convert.ToInt32(xCentro*dim);
            this.yCentro =Convert.ToInt32( yCentro*dim);
            this.raggio = Convert.ToInt32(raggio*dim);
            //this.testo = testo;     // carattere o tipo di scrittura (es times new roman)???
            //this.f = f;
            f = new Font(FontFamily.GenericSansSerif, dimcar*dim);
            stato = Stato.Inattivo;
            testoSL = testo.ToUpper();  //??
            perimetro = new GraphicsPath();
            perimetro.AddEllipse(xCentro - raggio, yCentro - raggio, 2 * raggio, 2 * raggio);      //?? a perch� serve un ellisse? e i parametri?
            Centro = new Point(xCentro, yCentro);
        }

        //funziona utilizzata per disegnare il tasto quando si solleva l'evento Paint
        public void disegna(Graphics dove, Color inattivo, Color temporaneo, Color attivo, Color trasparente, SolidBrush colorcontornobott)       // x disegn 1 tasto rotondo (nella classe form c� il foreach ke permettera di disegn tutti)
        {                                   // inattivo = colore del tasto quando � inattivo (settato nel file config.cfg)
            switch (stato)
            {
                case Stato.Inattivo:        //Stato.Inattivo torna un numero, sarebbe quando non ci 6 sopra col mouse
                    dimensioneTesto = dove.MeasureString(testo, f);
                    dove.FillPath(new SolidBrush(inattivo), perimetro);         // gli dai il perimetro dell'oggetto da riempire (in sto caso colora tt ci� ke � dentro il perimetro)
                    posizioneTesto.X = xCentro - (dimensioneTesto.Width / 2);        // con questa istruz + qll successiva posizioni il carattere esattamente al centro del cerchio
                    posizioneTesto.Y = yCentro - (dimensioneTesto.Height / 2);
                    if (Shift.tastoAttivo == 0 && Lock.tastoAttivo == 0)           // se il lock o shift � non � attivo mando il testo normale... 
                        dove.DrawString(testo, f, new SolidBrush(coloreFont), posizioneTesto);
                    else
                    {
                        //AAA
                        string testoShift = testo.ToUpper();                  //...altrimenti uso la funz ToUpper() per renderlo maiuscolo (ha creato 2 classi apposite per shift e lock
                        dimensioneTesto = dove.MeasureString(testoShift, f);
                        dove.FillPath(new SolidBrush(inattivo), perimetro);         // gli dai il perimetro dell'oggetto da riempire (in sto caso colora tt ci� ke � dentro il perimetro)
                        posizioneTesto.X = xCentro - (dimensioneTesto.Width / 2);        // con questa istruz + qll successiva posizioni il carattere esattamente al centro del cerchio
                        posizioneTesto.Y = yCentro - (dimensioneTesto.Height / 2);
                        dove.DrawString(testoShift, f, new SolidBrush(coloreFont), posizioneTesto.X, posizioneTesto.Y);
                    }
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetro); // con qst istruz coloro il perimetro del tasto rotondo in nero e con spessore = 2 
                    break;
                case Stato.Attivo:      // quando c 6 sopra col mouse
                    //Cursor.Current = Form1.getCustomCursor();

                    dove.FillPath(new SolidBrush(attivo), perimetro);
                    if (Shift.tastoAttivo == 0 && Lock.tastoAttivo==0)
                        dove.DrawString(testo, f, new SolidBrush(coloreFont), posizioneTesto);   //serve a disegnare il carattere dentro il perimetro
                    else
                    {
                        string testoShift = testo.ToUpper();
                        dimensioneTesto = dove.MeasureString(testoShift, f);
                        dove.FillPath(new SolidBrush(attivo), perimetro);         // gli dai il perimetro dell'oggetto da riempire (in sto caso colora tt ci� ke � dentro il perimetro)
                        posizioneTesto.X = xCentro - (dimensioneTesto.Width / 2);        // con questa istruz + qll successiva posizioni il carattere esattamente al centro del cerchio
                        posizioneTesto.Y = yCentro - (dimensioneTesto.Height / 2);
                        dove.DrawString(testoShift, f, new SolidBrush(coloreFont), posizioneTesto);
                    }
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetro);  
                    break;
                case Stato.Trasparente:
                    dove.FillPath(new SolidBrush(trasparente), perimetro);
                    dove.DrawString(testo, f, new SolidBrush(trasparente), posizioneTesto);
                    dove.DrawPath(new Pen(new SolidBrush(trasparente), 2), perimetro); // senza questa rimane 1 p� + spesso, cambia poco se la togli
                    dove.DrawPath(new Pen(colorcontornobott, 1), perimetro);  
                    break;
                case Stato.Temporaneo:      //A COSA CORRISP STO STATO?? se ci 6 sopra col mouse
                    dove.FillPath(new SolidBrush(temporaneo), perimetro);
                    if (Shift.tastoAttivo == 0 && Lock.tastoAttivo == 0)
                        dove.DrawString(testo, f, new SolidBrush(coloreFont), posizioneTesto);
                    else
                    {
                        string testoShift = testo.ToUpper();
                        dimensioneTesto = dove.MeasureString(testoShift, f);
                        dove.FillPath(new SolidBrush(inattivo), perimetro);         // gli dai il perimetro dell'oggetto da riempire (in sto caso colora tt ci� ke � dentro il perimetro)
                        posizioneTesto.X = xCentro - (dimensioneTesto.Width / 2);        // con questa istruz + qll successiva posizioni il carattere esattamente al centro del cerchio
                        posizioneTesto.Y = yCentro - (dimensioneTesto.Height / 2);
                        dove.DrawString(testoShift, f, new SolidBrush(coloreFont), posizioneTesto);
                    }
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetro);
                    break;
            }                      
        }

        //funzione per modificare lo stato del tasto in base alla posizione del puntatore
        public void modificaTasto(int xPosizione, int yPosizione)
        {
            posizioneCorrente = new Point(xPosizione, yPosizione);      // pos del puntatore?
            // perch� tasti attivi � una funzione statica

            if (stato == Stato.Trasparente && (tastiAttivi == 0 && TastiEsagonali.tastiAttivi==0 && Space.tastiAttivi==0 &&  BackSpace.tastoAttivo==0))
                stato = Stato.Inattivo;     // se ptatore nn � sopra alcun tasto --> � inattivo
            if (TastiEsagonali.tastiAttivi == 1)     // se il ptatore � nel tasto esagonale --> tasto rotondo � inattivo
                stato = Stato.Inattivo;
            /*else if (perimetro.IsVisible(posizioneCorrente))     // IsVisible (posizioneCorrente) --> se il puntatore � dentro il perimetro di un tasto rotondo
            {
                if (tastiTemporanei == 0 && stato == Stato.Inattivo) // se nn ci sono altri tasti temporanei e se stato era inattivo --> temporaneo
                {
                    stato = Stato.Temporaneo;
                    tastiTemporanei = 1;
                }
            }*/
            else
            {
                // se ptatore nn � nel tasto
                if (stato == Stato.Temporaneo)
                {
                    stato = Stato.Inattivo;
                    tastiTemporanei = 0;
                }

                if (stato == Stato.Attivo)
                {
                    tastiAttivi = 0;
                    stato = Stato.Inattivo;
                }
            }
        }

        //funzioni per cambiare lo stato del tasto
        public void rendiTrasparente()
        {
            ; //XYXYXY stato = Stato.Trasparente;
        }

        public void attivaTasto()
        { 
            Cursor.Current = Form1.getCustomCursor();
            tastiAttivi = 1;
            stato = Stato.Attivo;     
            tastiTemporanei = 0;           
        }
        public void disattiva()
        {
            tastiAttivi = 0;
            stato = Stato.Inattivo;
        }
    }
}
