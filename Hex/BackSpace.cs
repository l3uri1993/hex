using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Hex
{
    public class BackSpace
    {
        public int xCentro, yCentro;
        int deltaX, deltaY;
        public enum Stato { Attivo, Inattivo, Trasparente, Temporaneo }
        public Stato stato;
        public GraphicsPath perimetro;
        public Font f;
        Point Centro;
        Point vertice1,vertice2,vertice3,vertice4;
        public SizeF dimensioneTesto;
        public PointF posizioneTesto = new PointF();
        Point posizioneCorrente;                        // come il lock e lo shift tranne sta istruz
        public static int tastoAttivo = 0;
        public string testo;

        Color coloreFont = Form1.getColoreFont();   // usata per tornare il coloreFont comune a tutti i tasti preso da Form1 in leggiConfigurazione


        //costruttore utilizzato per formare il tasto
        public BackSpace(int xCentro, int yCentro, int deltaX, int deltaY, Font f, string testo)
        {
            this.xCentro = xCentro;
            this.yCentro = yCentro;
            this.deltaX = deltaX;
            this.deltaY = deltaY;
            this.testo=testo;
            this.f = f;
            stato = Stato.Inattivo;
            perimetro = new GraphicsPath();
            vertice1 = new Point(xCentro - deltaX / 2, yCentro - deltaY / 2);
            vertice2 = new Point(xCentro + deltaX / 2, yCentro - deltaY / 2);
            vertice3 = new Point(xCentro + deltaX / 2, yCentro + deltaY / 2);
            vertice4 = new Point(xCentro - deltaX / 2, yCentro + deltaY / 2);
            perimetro.AddLine(vertice1, vertice2);
            perimetro.AddLine(vertice2, vertice3);
            perimetro.AddLine(vertice3, vertice4);
            perimetro.AddLine(vertice4, vertice1);
            Centro = new Point(xCentro, yCentro);
        }

        public void ModTastBackSpace(float dim, float dimcar)
        {
            this.xCentro =Convert.ToInt32(xCentro*dim);
            this.yCentro =Convert.ToInt32( yCentro*dim);
            this.deltaX = Convert.ToInt32(deltaX*dim);
            this.deltaY =Convert.ToInt32( deltaY*dim);
            f = new Font(FontFamily.GenericSansSerif, dimcar * dim);
            perimetro = new GraphicsPath();
            vertice1 = new Point(xCentro - deltaX / 2, yCentro - deltaY / 2);
            vertice2 = new Point(xCentro + deltaX / 2, yCentro - deltaY / 2);
            vertice3 = new Point(xCentro + deltaX / 2, yCentro + deltaY / 2);
            vertice4 = new Point(xCentro - deltaX / 2, yCentro + deltaY / 2);
            perimetro.AddLine(vertice1, vertice2);
            perimetro.AddLine(vertice2, vertice3);
            perimetro.AddLine(vertice3, vertice4);
            perimetro.AddLine(vertice4, vertice1);
            Centro = new Point(xCentro, yCentro);
        }


        //funziona utilizzata per disegnare il tasto quando si solleva l'evento Paint
        public void disegna(Graphics dove, Color inattivo, Color temporaneo, Color attivo, Color trasparente, SolidBrush colorcontornobott)
        {
            switch (stato)
            {
                case Stato.Inattivo:    // quando nn 6 sopra al tasto e neanke sopra gli esagonali dopo il T1 in cui vai nel menu di preselez... x� nn � cs� xk� se entri nella finestra torna normale
                    dimensioneTesto = dove.MeasureString(testo, f);                  // calcola la dimens del testo passandogli il carattere "del" e il font
                    dove.FillPath(new SolidBrush(inattivo), perimetro);             // colora dentro il perimetro
                    
                    posizioneTesto.X = xCentro - (dimensioneTesto.Width / 2);        // posiz del testo, la crea qui perch� inizialm tt i tasti sono inattivi quindi lo crea appena apri la finestra
                    posizioneTesto.Y = yCentro - (dimensioneTesto.Height / 2);

                    dove.DrawString(testo, f, new SolidBrush(coloreFont), posizioneTesto);       // disegna il "del" dentro il tasto
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetro);            // colora di nero il perimetro del rettangolo
                    break;
                case Stato.Attivo:  // appena ci vai sopra col mouse
                    //Cursor.Current = Form1.getCustomCursor();

                    dove.FillPath(new SolidBrush(attivo), perimetro);       // uguale al prec ma con colore attivo
                    dove.DrawString(testo, f, new SolidBrush(coloreFont), posizioneTesto);
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetro);
                    break;
                case Stato.Trasparente:     // stato che si attiva quando 6 SOLO su un tasto esagonale
                    dove.FillPath(new SolidBrush(trasparente), perimetro);
                    dove.DrawString(testo, f, new SolidBrush(trasparente), posizioneTesto);
                    dove.DrawPath(new Pen(new SolidBrush(trasparente), 2), perimetro);          // c� sta funzione in + ke ripassa un pochino il perimetro cn 1 altro colore, non molto importante
                    dove.DrawPath(new Pen(colorcontornobott, 1), perimetro);
                    break;
                case Stato.Temporaneo:  //??
                    dove.FillPath(new SolidBrush(temporaneo), perimetro);       // uguale ai primi 2
                    dove.DrawString(testo, f, new SolidBrush(coloreFont), posizioneTesto);
                    dove.DrawPath(new Pen(colorcontornobott, 2), perimetro);
                    break;
            }    
        }


        //funzione per modificare lo stato del tasto in base alla posizione del puntatore
        public void modificaTasto(int xPosizione, int yPosizione)       //riceve come parametri la pos del puntatore
        {
            posizioneCorrente = new Point(xPosizione, yPosizione);
            /*if (stato == Stato.Inattivo && TastiEsagonali.tastiAttivi == 1)     //perch� nn verifica se anche i tasti rotondi nn sono attivi???
                //XYXYXY stato = Stato.Trasparente;*/
            
            if (stato == Stato.Trasparente && (TastiEsagonali.tastiAttivi == 0 && TastiRotondi.tastiAttivi==0 && Space.tastiAttivi==0))
                stato = Stato.Inattivo;
            if (TastiEsagonali.tastiAttivi == 1)
                stato = Stato.Inattivo;
            else if (perimetro.IsVisible(posizioneCorrente))
            {
                if (stato == Stato.Inattivo)
                {
                    stato = Stato.Temporaneo;
                }
            }
            else
            {
                if (stato == Stato.Temporaneo)
                {
                    stato = Stato.Inattivo;
                }
                if (stato == Stato.Attivo)
                {
                    stato = Stato.Inattivo;
                    tastoAttivo = 0;
                }
            }
        }


        //funzioni per cambiare lo stato del tasto
        public void rendiTrasparente()
        {
            ; //XYXYXY stato = Stato.Trasparente;
        }

        public void attivaTasto()
        {
            //Cursor.Current = Form1.getCustomCursor();

            stato = Stato.Attivo;
            tastoAttivo = 1;
        }
        public void disattiva()
        {
            stato = Stato.Inattivo;
            tastoAttivo = 0;
        }

    }
}



