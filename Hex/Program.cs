﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Hex
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]         //Gli attributi STAThread e MTAThread specificano il modello di threading da utilizzare nella gestione degli oggetti COM. 
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
