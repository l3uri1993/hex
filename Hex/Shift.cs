using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Hex
{
    public class Shift
    {
        public int xCentro, yCentro;            // uguale identico al lock
        int deltaX, deltaY;
        public enum Stato { Attivo, Inattivo, Trasparente, Temporaneo }
        public Stato stato;
        public GraphicsPath perimetro;
        public Font f;
        Point Centro;
        Point vertice1, vertice2, vertice3, vertice4;
        public SizeF dimensioneTesto;
        public PointF posizioneTesto = new PointF();
        
        public static int tastoAttivo = 0;  //--> flagShift = false
        public static int tastoAttiv = 0;   // corrisponde allo stato ( attivo, inattivo, temporaneo, trasparente)

        public string testo;

        Point posizioneCorrente;                        // come il lock e lo shift tranne sta istruz

        //costruttore utilizzato per formare il tasto
        public Shift(int xCentro, int yCentro, int deltaX, int deltaY, Font f, string testo)
        {
            this.xCentro = xCentro;
            this.yCentro = yCentro;
            this.deltaX = deltaX;
            this.deltaY = deltaY;
            this.testo=testo;
            this.f = f;
            stato = Stato.Inattivo;
            perimetro = new GraphicsPath();
            vertice1 = new Point(xCentro - deltaX / 2, yCentro - deltaY / 2);
            vertice2 = new Point(xCentro + deltaX / 2, yCentro - deltaY / 2);
            vertice3 = new Point(xCentro + deltaX / 2, yCentro + deltaY / 2);
            vertice4 = new Point(xCentro - deltaX / 2, yCentro + deltaY / 2);
            perimetro.AddLine(vertice1, vertice2);
            perimetro.AddLine(vertice2, vertice3);
            perimetro.AddLine(vertice3, vertice4);
            perimetro.AddLine(vertice4, vertice1);
            Centro = new Point(xCentro, yCentro);
        }

        public void ModTastShift(float dim, float dimcar)
        {
            this.xCentro = Convert.ToInt32(xCentro*dim);
            this.yCentro = Convert.ToInt32(yCentro*dim);
            this.deltaX = Convert.ToInt32(deltaX*dim);
            this.deltaY = Convert.ToInt32(deltaY*dim);
            f = new Font(FontFamily.GenericSansSerif, dimcar * dim);
            perimetro = new GraphicsPath();
            vertice1 = new Point(xCentro - deltaX / 2, yCentro - deltaY / 2);
            vertice2 = new Point(xCentro + deltaX / 2, yCentro - deltaY / 2);
            vertice3 = new Point(xCentro + deltaX / 2, yCentro + deltaY / 2);
            vertice4 = new Point(xCentro - deltaX / 2, yCentro + deltaY / 2);
            perimetro.AddLine(vertice1, vertice2);
            perimetro.AddLine(vertice2, vertice3);
            perimetro.AddLine(vertice3, vertice4);
            perimetro.AddLine(vertice4, vertice1);
            Centro = new Point(xCentro, yCentro);
        }


        public void modificaTasto(int xPosizione, int yPosizione)       //riceve come parametri la pos del puntatore
        {
            posizioneCorrente = new Point(xPosizione, yPosizione);
            /*if (stato == Stato.Inattivo && TastiEsagonali.tastiAttivi == 1)     //perch� nn verifica se anche i tasti rotondi nn sono attivi???
                ; //XYXYXY stato = Stato.Trasparente;*/
            if (Lock.tastoAttivo == 1)
                stato = Stato.Inattivo;
            if (stato == Stato.Trasparente && (TastiEsagonali.tastiAttivi == 0 && TastiRotondi.tastiAttivi == 0 && Space.tastiAttivi == 0))
                stato = Stato.Inattivo;
            if (TastiEsagonali.tastiAttivi == 1)
                stato = Stato.Inattivo;
            else if (perimetro.IsVisible(posizioneCorrente))
            {
                if (stato == Stato.Inattivo)
                {
                    stato = Stato.Temporaneo;
                }
            }
            else if (perimetro.IsVisible(posizioneCorrente))
            {
            if (stato == Stato.Temporaneo)
                {
                    stato = Stato.Inattivo;
                }
                if (stato == Stato.Attivo)
                {
                    stato = Stato.Inattivo;
                    tastoAttiv = 0;
                }
            }
        }


        //funzioni per cambiare lo stato del tasto
        public void rendiTrasparente()
        {
            ; //XYXYXY stato = Stato.Trasparente;
        }

        public void rendiTemporaneo()   // metodo per risolvere pto 4 curat
        {
            stato = Stato.Temporaneo;
        }

        public void attivaTasto()
        {
            //Cursor.Current = Form1.getCustomCursor();

            stato = Stato.Attivo;
            tastoAttiv = 1;      // SE LASCIO STA ISTRUZ, DOPO TIMER1 SE MI MUOVO ANCORA SULLO SHIFT, OGNI MOVIM RILEVATO CAMBIA LE LETTERE DA MINUSCOLA A MAIUSCOLA
            
        }
        
        public void disattiva()     // sembrerebbe non vista dal programma
        {
            stato = Stato.Inattivo;
            tastoAttiv = 0;
        }
        // MA DOVE LO IMPLEMENTA?? (POS CARATTERE NEL RETTANGOLO ECC) non ce n � stato bisogno, usera il visualizzaLock
    }
}
